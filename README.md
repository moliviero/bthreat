# BThreat#

BThreat is a C# event driven [behaviour tree](http://www.gamasutra.com/blogs/ChrisSimpson/20140717/221339/Behavior_trees_for_AI_How_they_work.php) framework.

It's written in C#, but a C++ port of the core system is planned.

The framework is currently composed of several components:

* The **core library**: with the scheduler, nodes, tasks and blackboards. This is the core module, and can be used inside any C# game.
* The **Unity3D extension**: is a set of extensions to the core library and specific nodes, that facilitate and extend the system for an easy integration inside the engine.
* **UI and editor**: this is an extension for Unity3D that provides a graphical interface to build the trees and save/serialize them.
* **Debugger**: the editor tool can be used to inspect the tree evaluation in real time.

### Goal ###

When I approached the behaviour tree topic, I found many resources and documents about the theory and the current research (see. [AiGameDev](aigamedev.com) for many articles on this topic).
There are many implementation out there, and some good example of code to start.

When I started to write BThreat's code I realized that there were much more to do, in order to obtain something usable inside a real game. I had to experiment a lot and take some design decisions. 
I hope the whole code can help other people interested in behaviour tree, giving them an example of a possible implementation.

### Disclaimer ###

The library has been developed and used inside an independent mobile game. Despite it has been designed carefully in many parts, until now has been a closed project, not documented and developed only for the part that were relevant to the game development I was involved.
It's quite flexible, but should be used with care: contains several know bugs and limitations. 

### How do I get set up? ###

The easiest way is to download all the repository contents inside a new Unity3D project and start playing around with the rudimental example scene.
For now you have to dig inside the code to understand it's usage. I will try to provide some examples.


### License ###

BThreat is released under GPLv3.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. 

### Contribution guidelines ###

I'd like to open to contribution from the community as soon as possible. But the code needs some clean-up, documentation and guide lines before that. 
Any feedback or bug report is really appreciated.


Have Fun!

Contacts: bthreatframework@gmail.com