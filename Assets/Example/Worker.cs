﻿using UnityEngine;
using System.Collections;

using BT.Unity;

public class Worker : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () { 
	
	}

    [ActionRoutineTick("Idle")]
    public IEnumerator IdleAction()
    {
        while (true)
        {
            transform.Rotate(transform.up, 30f * Time.deltaTime);
            yield return BTDefs.RUNNING;
        }
    }
}
