/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace EditorExtension
{
	public static class EditorUtils
	{
		public static string GetMeshFilterGUID(MeshFilter filter)
		{
			return AssetDatabase.AssetPathToGUID(
				AssetDatabase.GetAssetPath(
					PrefabUtility.GetPrefabParent(filter)));

		}
		public static string GetObjectGUID(UnityEngine.Object obj)
		{
			return AssetDatabase.AssetPathToGUID(
				AssetDatabase.GetAssetPath(
				PrefabUtility.GetPrefabParent(obj)));
			
		}
		public static void Space(int n)
		{
			for (int i = 0 ; i < n ; ++i)
			{
				EditorGUILayout.Space();
			}
		}
		public static Mesh CreateOrReplaceAsset(Mesh mesh, string path)
		{
			var meshAsset = AssetDatabase.LoadAssetAtPath (path, typeof(Mesh)) as Mesh;
			if (meshAsset == null) {
				meshAsset = Mesh.Instantiate(mesh) as Mesh; // new Mesh ();
				EditorUtility.CopySerialized (mesh, meshAsset);
				AssetDatabase.CreateAsset (meshAsset, path);
			} else {
				EditorUtility.CopySerialized (mesh, meshAsset);
				AssetDatabase.SaveAssets ();
			}
			return AssetDatabase.LoadAssetAtPath (path, typeof(Mesh)) as Mesh;
		}

        public static ASSET_TYPE CreateAssetAtPath<ASSET_TYPE>(string assetPath)
            where ASSET_TYPE : ScriptableObject
        {
     
            ASSET_TYPE asset = ScriptableObject.CreateInstance<ASSET_TYPE>();

            AssetDatabase.CreateAsset(asset, assetPath);

            AssetDatabase.SaveAssets();
            
            return asset;
        }
		public static ASSET_TYPE CreateAssetAtCurrentPath<ASSET_TYPE>(string name) 
			where ASSET_TYPE : ScriptableObject
		{
			string currDir = AssetDatabase.GetAssetPath(Selection.activeObject);

			if (currDir == null || currDir == "")
			{
				currDir = "Assets";
			}
			else if (Path.GetExtension (currDir) != "") 
			{
				currDir = currDir.Replace (Path.GetFileName (currDir), "");
			}

			string assetPath = AssetDatabase.GenerateUniqueAssetPath (currDir + "/" + name + ".asset");

			ASSET_TYPE asset = ScriptableObject.CreateInstance<ASSET_TYPE>();

			AssetDatabase.CreateAsset(asset,assetPath);

			AssetDatabase.SaveAssets();
		//	EditorUtility.FocusProjectWindow();
			Selection.activeObject = asset;

			return asset;
		}
        [MenuItem("Utils/Editor/Clear Player Prefs")]
        public static void ClearPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }
        private static void RevertMeshRendererMaterialList(MeshRenderer mr)
        {
            SerializedObject obj = new SerializedObject(mr);

            SerializedProperty mats = obj.FindProperty("m_Materials");

            if (PrefabUtility.GetPrefabParent(mr.gameObject) != null)
            {
                PrefabType t = PrefabUtility.GetPrefabType(mr.gameObject);
                if (t == PrefabType.DisconnectedPrefabInstance || t == PrefabType.DisconnectedModelPrefabInstance)
                {
                    Debug.LogError(mr.name + " is not connected skip");

                }
                else
                {
                    Undo.RecordObject(mr, "revert material list to prefab");
                    mats.prefabOverride = false;
                    mats.serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                Debug.LogError(mr.name + " is not e prefab skip");
            }

        }
        private static void RevertMaterialsToPrefab(GameObject[] targets)
        {
            foreach (var g in targets)
            {

                MeshRenderer mr = g.GetComponent<MeshRenderer>();
                if (mr != null)
                    RevertMeshRendererMaterialList(mr);
                else
                    Debug.LogError(mr.name + " has no attached MeshRenderer: SKIPPED");
            }
        }

        [MenuItem("Utils/Prefabs/Revert Material To Prefab")]
        public static void RevertMaterialToPrefab()
        {
            RevertMaterialsToPrefab(Selection.gameObjects);

        }
        [MenuItem("CONTEXT/MeshRenderer/Revert Material To Prefab")]
        public static void RevertMaterialToPrefabContext(MenuCommand cmd)
        {
            MeshRenderer mr = cmd.context as MeshRenderer;
            Debug.Log(mr.GetInstanceID());
            RevertMeshRendererMaterialList(mr);

        }
	}
}

