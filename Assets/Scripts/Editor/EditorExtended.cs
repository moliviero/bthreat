﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using UnityEditor;

namespace EditorExtension
{
    public class SerializedObjectExt 
    {
        public readonly SerializedObject m_serializedObj;

        public SerializedObjectExt(SerializedObject obj)
        {
            m_serializedObj = obj;
        }
        public SerializedProperty this[string path]
        {
            get
            {
                return m_serializedObj.FindProperty(path);
            }
          
        }
    }
    public class EditorExtended<T> : Editor
        where T : UnityEngine.Object
    {

        public T Target
        {
            get { return target as T; }
        }
        public SerializedObjectExt SerializedObject
        {
            get { return new SerializedObjectExt(serializedObject); }
        }
        protected virtual void OnEnable()
        { 
        }
    }
}