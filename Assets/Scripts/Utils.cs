/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

	
public struct Circle
{
	public float radius;
	public Vector3 center; 
}

public struct TransformStruct
{
	public Vector3 position;
	public Quaternion rotation;
	public Vector3 scale;
	
}
public class Pair<TFirst, TSecond>
{
	public TFirst First{get;set;}
	public TSecond Second{get;set;}
	
	public Pair(TFirst first, TSecond second)
	{
		First = first;
		Second = second;
	}
}
public enum AngleDir {CLOCK, ANTICLOCK, FORWARD};
public class Utils
{
	public const float MIN_ANGLE = 0.0001f;
	
	public static AngleDir GetAngleDirection(Vector3 beginDir, Vector3 endDir, Vector3 upDir)
	{
		Vector3 cross = Vector3.Cross(beginDir, endDir);
		float dotCheck = Vector3.Dot(beginDir,endDir);
		
		if (cross.magnitude  < MIN_ANGLE)
		{
			if ( dotCheck < -1.0f + MIN_ANGLE)
			{
				return AngleDir.CLOCK;
			}
			return AngleDir.FORWARD;
		}
			
		
		float dot = Vector3.Dot(cross, upDir);
		if (dot > 0.0f)
			return AngleDir.CLOCK;
		else if (dot < 0.0f)
			return AngleDir.ANTICLOCK;
		return AngleDir.FORWARD;
	}
	
	public static float GetRealAngle(Vector3 beginDir, Vector3 endDir, Vector3 upDir)
	{
		AngleDir dir = GetAngleDirection(beginDir, endDir, upDir);
		
		float angle = Vector3.Angle(beginDir, endDir);
		if (dir ==AngleDir.CLOCK)
		{
			return angle;
		}else if (dir ==AngleDir.ANTICLOCK)
		{
			return 360.0f - angle;
		}else
		{
			return 0.0f;
		}
	}
	
//	public static Rect GetMinRectContaingPoints(IList<Vector3> pointSet)
//	{
//		float minX = float.MaxValue;
//		float minZ = float.MaxValue;
//		float maxX = float.MinValue;
//		float maxZ = float.MinValue;
//		
//		foreach (Vector3 point in pointSet)
//		{
//			minX = Mathf.Min(point.x, minX);
//			minZ = Mathf.Min(point.z, minZ);
//			maxX = Mathf.Max(point.x, maxX);
//			maxZ = Mathf.Max(point.z, maxZ);	
//		}
//		float height = Vector2.Distance( new Vector2(minZ,0), new Vector2(maxZ,0)); 
//		float width = Vector2.Distance( new Vector2(minX,0), new Vector2(maxX,0)); 
//		
//		Rect r = new Rect(minX, maxZ,width,height );
//		
//		return r;
//		
//	}
	public static float GetMinDistance(IList<Vector3> pointSet, Vector3 target)
	{
		float minDist = float.MaxValue;
		
		foreach (Vector3 point in pointSet)
		{
			minDist = Mathf.Min(minDist, Vector3.Distance(point, target));
		}
		return minDist;
	}
	public static float GetMaxDistance(IList<Vector3> pointSet, Vector3 target)
	{
		float maxDist = float.MinValue;
		
		foreach (Vector3 point in pointSet)
		{
			maxDist = Mathf.Max(maxDist, Vector3.Distance(point, target));
		}
		return maxDist;
	}
	public static Vector3 GetCentroid(IList<Vector3> convexPointSet)
	{
		Vector3 ret = Vector3.zero;
		foreach(Vector3 point in convexPointSet)
		{
			ret += point;
		}
		ret /= convexPointSet.Count;
		return ret;
		
	}
	public static Circle GetMinInscribedCirlce(IList<Vector3> pointSet)
	{
		Circle circle;
		//Rect rect = GetMinRectContaingPoints(pointSet);
		
		circle.center = GetCentroid(pointSet);//new Vector3 (rect.center.x, 0.0f, rect.center.y);
		circle.radius = GetMinDistance(pointSet,circle.center);//Mathf.Min(rect.width, rect.height)  * 0.5f;
		return circle;
	}
	public static Circle GetMaxInscribedCirlce(IList<Vector3> pointSet)
	{
		Circle circle;
		//Rect rect = GetMinRectContaingPoints(pointSet);
		
		circle.center = GetCentroid(pointSet);//new Vector3 (rect.center.x, 0.0f, rect.center.y);
		circle.radius = GetMaxDistance(pointSet,circle.center);//Mathf.Min(rect.width, rect.height)  * 0.5f;
		return circle;
	}
	//--------------- reference: 3d math primer page 287 -----------------------------
	public static Vector3 RayCircleIntersectionPoint(Vector3 circleCenter, float radius, Vector3 rayStartPoint, Vector3 rayDir)
	{
		Vector3 e = circleCenter - rayStartPoint;
		float a = Vector3.Dot(e,rayDir);
		
		float f_squared = (radius * radius) - Vector3.Dot (e , e) + (a * a);
		if (f_squared <= 0)
		{
			throw new Exception("No intersection");
		}
		
		float f = Mathf.Sqrt( f_squared);
		float t = a - f;
		
		Vector3 intersectionPoint = rayStartPoint + rayDir *t;
		
		return intersectionPoint;
	}
	public static void DumpTransformLocal(Transform transform, ref TransformStruct obj)
	{
		obj.position = transform.localPosition;
		obj.rotation = transform.localRotation;
		obj.scale = transform.localScale;
	}
	
	private static IList<Vector3> TransformToGlobalSpace (IList<Vector3> localSpaceList, Transform transform)
	{
		return localSpaceList.Select(item => transform.TransformPoint(item)).ToList();
	}
	
	public static bool IsUnderMinDistanceThreshold(Vector3 first, Vector3 second, float threshold)
	{
		return Vector3.Distance(first, second) < threshold;
	}
	public static bool IsUnderMinDistanceThreshold(Vector2 first, Vector2 second, float threshold)
	{
		return Vector2.Distance(first, second) < threshold;
	}
	
	public static Type GetRandomObjectFromList<Type>(IList<Type> list)
	{
		if (list.Count == 0)
			return default(Type);
		
		int max = list.Count - 1;
		int rand = UnityEngine.Random.Range(0,max);
		
		return list[rand];
	}
	public static IFace[] GetComponentsByInterface<IFace>(GameObject go)
	{
		return go.GetComponents<Component>().Where(c => c is IFace).Cast<IFace>().ToArray();
	}
	public static IFace GetComponentByInterface<IFace>(GameObject go) where IFace : Component 
	{
		return go.GetComponents<Component>().Where(c => c is IFace).First() as IFace;
	}
	public static UnityEngine.Object GetComponentByInterface(GameObject go, Type t)
	{
		return go.GetComponents<Component>().Where(c => t.IsAssignableFrom(c.GetType())).First();
	}
	public static UnityEngine.Object GetComponentByInterface(GameObject go, Type[] t)
	{
		return go.GetComponents<Component>().Where(c =>  t.All(iface => iface.IsAssignableFrom(c.GetType()))).First();
	}
}


 