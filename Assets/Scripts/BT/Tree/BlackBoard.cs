﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class BlackBoard : IBlackBoard
	{
        private Dictionary<int, float> m_floatMap;
        private Dictionary<int, bool> m_boolMap;
        private Dictionary<int, int> m_intMap;
        private Dictionary<int, object> m_objMap;

        private Dictionary<int, BBValueObserver> m_observers;

        public BlackBoard()
        {
            m_boolMap = new Dictionary<int, bool>();
            m_floatMap = new Dictionary<int, float>();
            m_intMap = new Dictionary<int, int>();
            m_objMap = new Dictionary<int, object>();

            m_observers = new Dictionary<int, BBValueObserver>();
        }
        public int PropertyToId(string prop)
        {
            return prop.GetHashCode();
        }

        public void SetFloat(int id, float val)
        {
            m_floatMap[id] = val;

            NotifyObservers(id);
        }

        public float GetFloat(int id)
        {
            float ret;

            if (m_floatMap.TryGetValue(id, out ret))
            {
                return ret;
            }
            return default(float);
        }

        public void SetBool(int id, bool val)
        {
            m_boolMap[id] = val;

            NotifyObservers(id);
        }

        public bool GetBool(int id)
        {
            bool ret;
            if(m_boolMap.TryGetValue(id,out ret))
            {
                return ret;
            }
            return default(bool);
        }

        public void SetInt(int id, int val)
        {
            m_intMap[id] = val;

            NotifyObservers(id);
        }

        public int GetInt(int id)
        {
            int ret;
            if(m_intMap.TryGetValue(id,out ret))
            {
                return ret;
            }
            return default(int);
        }

        public void SetObject<IType>(int id, IType val)
            where IType : class
        {
            object obj;
            if(m_objMap.TryGetValue(id, out obj))
            {
                if (!(obj is IType))
                {
                    throw new ArgumentException("BlackBoard::SetObject given type " + typeof(IType) + " but stored " + obj.GetType());
                }
            }
            m_objMap[id] = val;

            NotifyObservers(id);
        }

        public IType GetObject<IType>(int id)
             where IType : class
        {
            object ret;

            if (m_objMap.TryGetValue(id, out ret))
            {
                return ret as IType;
            }
            return null;
        }

        public void Clear()
        {
            m_boolMap.Clear();
            m_intMap.Clear();
            m_objMap.Clear();
            m_floatMap.Clear();
        }


        public void ObserverSubscribe(int propId,BBValueObserver observer)
        {
            BBValueObserver observerList;

            if (m_observers.TryGetValue(propId, out observerList))
            {
                m_observers[propId] = observerList + observer;
            }
            else
            {
                m_observers[propId] = observer;
            }
        }

        public void ObserverUnsubscribe(int propId, BBValueObserver observer)
        {
            BBValueObserver observerList;

            if (m_observers.TryGetValue(propId, out observerList))
            {

                m_observers[propId] = observerList - observer;
            }
         
        }
        private void NotifyObservers(int id)
        {
            BBValueObserver observer;

            if (m_observers.TryGetValue(id, out observer))
            {
                if(observer != null)
                    observer(this, id); 
            }
        }
    }
}
