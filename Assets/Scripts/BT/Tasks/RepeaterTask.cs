﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using BT.Internals;

namespace BT
{
    public abstract class RepeaterTask : AbstractTask
	{
        protected IRepeater m_node;
        protected ITask m_childTask;
        protected ITree m_tree;

        public RepeaterTask(ITree tree,IRepeater node)
        {
            m_tree = tree;
            m_node = node;

          
        }
        public override void Accept(ITaskVisitor visitor)
        {
            throw new NotImplementedException();
        }

     
        public override INode SkeletonNode()
        {
            return m_node;
        }
    }
}
