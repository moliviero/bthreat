﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class ActionDelegateTask : AbstractTask
	{
        protected string m_actionName;
        protected IBlackBoard m_bb;

        protected Action m_node;

        private event TaskStartAction OnStart;
        private event TaskTickAction OnTick;
        private event TaskTerminateAction OnTerminate;

        public ActionDelegateTask(IBlackBoard bb, Action node)
        {
            m_node = node;
            m_bb = bb;
        }
        public override void Accept(ITaskVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Start()
        {
            string actName = m_node.GetActionName();
            int id_start = m_bb.PropertyToId(actName + Action.ACTION_START_SUFFIX);
            int id_tick = m_bb.PropertyToId(actName + Action.ACTION_TICK_SUFFIX);
            int id_terminate = m_bb.PropertyToId(actName + Action.ACTION_TERMINATE_SUFFIX);

            TaskStartAction startDelegate = m_bb.GetObject<TaskStartAction>(id_start);
            TaskTickAction tickDelegate = m_bb.GetObject<TaskTickAction>(id_tick);
            TaskTerminateAction terminateDelegate = m_bb.GetObject<TaskTerminateAction>(id_terminate);

            if (startDelegate != null)
                OnStart += startDelegate;
            else
                OnStart += delegate { };

            if(terminateDelegate != null)
                OnTerminate += terminateDelegate;
            else
                OnTerminate += delegate { };

            if (tickDelegate != null)
                OnTick += tickDelegate;
            else
                OnTick += () => { return Status.COMPLETED; };

            OnStart();
            _Started(this);
        }

        public override Status Tick()
        {
            m_status = OnTick();
            _Updated(this);
            
            return m_status;
        }

        public override void Terminate()
        {
            _Terminated(this);
            OnTerminate();
        
           
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
    }
}
