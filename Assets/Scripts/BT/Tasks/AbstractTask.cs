﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
    public abstract class AbstractTask : ITask
	{
        public event Action<ITask> Started = delegate{};
        public event Action<ITask> Updated = delegate{};
        public event Action<ITask> Terminated = delegate{};

        protected Status m_status;
        protected ITask m_parentTask;
        protected IScheduler m_scheduler;

        public Status GetStatus()
        {
            return m_status;
        }
        public void SetStatus(Status s)
        {
            m_status = s;
        }

        public ITask GetParent()
        {
            return m_parentTask;
        }
        public void SetParent(ITask parent)
        {
            m_parentTask = parent;
        }

        public void SetScheduler(IScheduler scheduler)
        {
            m_scheduler = scheduler;
        }

        public void SetVisitor(ITaskVisitor visitor)
        {
            throw new NotImplementedException();
        }

        public abstract void Accept(ITaskVisitor visitor);

        protected void _Terminated(ITask me)
        {
            Terminated(me);
        }
        protected void _Started(ITask me)
        {
            Started(me);
        }
        protected void _Updated(ITask me)
        {
            Updated(me);
        }

        public abstract void Start();
        public abstract Status Tick();
        public abstract void Terminate();

        public abstract INode SkeletonNode();




        public void ClearObservers()
        {
            Terminated = delegate { };
            Started = delegate { };
            Updated = delegate { };
        }
    }
}
