﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BT.Internals;

namespace BT
{

    public class ConditionTask : AbstractTask
	{
        
        protected Condition m_node;
        protected IBlackBoard m_bb;
        protected int m_bbId;
        protected ConditionStrategy m_cond;

        public ConditionTask(Condition cond, IBlackBoard bb, int bbId)
        {
            m_node = cond;
            m_bbId = bbId;
            m_bb = bb;

            m_cond = new ConditionStrategy(cond.GetComparerType(), cond.GetComparer(), bbId, bb, cond.GetRefValue());
        }
        public override void Accept(ITaskVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Start()
        {
            _Started(this);
        }

        public override Status Tick()
        {
            m_status =  m_cond.cond() == true ? Status.COMPLETED : Status.FAILED;
            
            return m_status;
        }

        public override void Terminate()
        {
            _Terminated(this);
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
    }
}
