﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
    public class InverterTask : AbstractTask
	{
        protected IInverter m_node;
        protected ITask m_currChildTask;
        protected ITree m_tree;
   
        public InverterTask(ITree bt,IInverter node)
        {
            m_node = node;
            m_tree = bt;
        }
        public override void Accept(ITaskVisitor visitor)
        {
            throw new NotImplementedException();
        }

        public override void Start()
        {
            _Started(this);
        }

        public override Status Tick()
        {
            if (m_status == Status.NONE)
            {
                InitChild();
                m_scheduler.Suspend(this);
                _Updated(this);
                return m_status;
            }
            Status childStat = m_currChildTask.GetStatus();
            _Updated(this);
            return childStat == Status.COMPLETED ? Status.FAILED : Status.COMPLETED;
           
            
        }
        private void InitChild()
        {
            m_currChildTask = m_node.GetChild(0).CreateTask(m_tree);
            m_currChildTask.SetParent(this);
            m_currChildTask.Terminated += OnChildTerminated;
            m_scheduler.Schedule(m_currChildTask);
        
        }
        public override void Terminate()
        {
            m_currChildTask.Terminated -= OnChildTerminated;
            _Terminated(this);
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
        public void OnChildTerminated(ITask child)
        {
            m_scheduler.Resume(this);
        }
    }
}
