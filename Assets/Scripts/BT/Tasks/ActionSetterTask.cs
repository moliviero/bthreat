﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class ActionSetterTask : AbstractTask
	{
        protected ActionSetter m_node;

      //  private int m_valueId;
        private System.Action m_setterDelegate;

        public ActionSetterTask(ActionSetter node, IBlackBoard bb)
        {
            m_node = node;
            BBValueType type = node.GetValueType();
            int id = bb.PropertyToId(node.GetValueName()); 
            switch (type)
            {
                case BBValueType.BOOL:
                {
                    bool b = Convert.ToBoolean(node.GetValue());
                    m_setterDelegate = () => { bb.SetBool(id, b); };
                    break;
                }
                case BBValueType.FLOAT:
                {
                    float f = Convert.ToSingle(node.GetValue());
                    m_setterDelegate = () => { bb.SetFloat(id, f); };
                    break;
                }
                case BBValueType.INT:
                {
                    int i = Convert.ToInt32(node.GetValue());
                    m_setterDelegate = () => { bb.SetInt(id, i); };
                    break;
                }
            }

        }
        public override void Accept(ITaskVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Start()
        {
            _Started(this);
        }

        public override Status Tick()
        {
            m_setterDelegate();
            _Updated(this);

            m_status = Status.COMPLETED;

            return m_status;
            //return Status.COMPLETED;
        }

        public override void Terminate()
        {
            _Terminated(this);
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
    }
}
