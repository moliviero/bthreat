﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace BT
{
    public class RandomSelectorTask : AbstractTask
    {
        protected RandomSelector m_node;

        protected int m_currChildNodeIndex;
        protected ITask m_currChildTask;
        protected Status m_currChildStatus;
        protected ITree m_tree;

        protected List<ChildEntry> m_availableChildren;
        protected float m_probTotal;

        private Random m_rand;
        protected class ChildEntry
        {
            public int index;
            public float prob;
            public ChildEntry(int i, float p)
            {
                index = i;
                prob = p;

            }
        }
        public RandomSelectorTask(ITree bt, RandomSelector node)
        {
            m_node = node;
            m_currChildStatus = Status.NONE;
            m_currChildNodeIndex = -1;
            m_currChildTask = null;
            m_tree = bt;

            m_rand = new Random();
            InitProbabilities();
        }
        private void InitProbabilities()
        {
            m_availableChildren = new List<ChildEntry>();

            float[] probs = m_node.Probabilities;
            m_probTotal = 0f;//; = m_node.ProbabilityTotal;
            
            for (int i = 0; i < m_node.ChildCount(); ++i)
            {
                m_probTotal += probs[i];
                m_availableChildren.Add(new ChildEntry(i, probs[i]));
            }
        }
        public override void Accept(ITaskVisitor visitor)
        {
            throw new System.NotImplementedException();
            //visitor.Visit(this);
        }

        public override void Start()
        {
            Debug.Assert(m_status == Status.RUNNING, "Start task in not a RUNNING status");

            _Started(this);
        }

        public override Status Tick()
        {
            Debug.Assert(m_status == Status.RUNNING, "SelectorTask::Tick in status" + m_status);

            if (m_currChildStatus != Status.COMPLETED && MoveNext())
            {
                m_scheduler.Suspend(this);
                return m_status;
            }
            m_status = m_currChildStatus;

            return m_status;
        }

        public override void Terminate()
        {
            _Terminated(this);
            CleanChildren();
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
        private void CleanChildren()
        {

            m_currChildTask.Terminated -= OnChildTerminated;

            if (!IsCompleted(m_currChildTask.GetStatus()))
            {
                m_scheduler.Halt(m_currChildTask);
            }

        }
        private bool IsCompleted(Status s)
        {
            return (s == Status.FAILED || s == Status.COMPLETED);
        }

        private bool MoveNext()
        {
            if (m_availableChildren.Count == 0)
                return false;

            float rand = (float) m_rand.NextDouble();

            int childIdx = 0;
            float acc = 0f;
            for (int i = 0; i < m_availableChildren.Count; ++i)
            {
                acc += m_availableChildren[i].prob / m_probTotal;
                if (rand < acc)
                {
                    childIdx = i;
                    break;
                }
            }
          

            if (m_currChildTask != null)
            {
                m_currChildTask.Terminated -= OnChildTerminated;
            }

            m_currChildNodeIndex = childIdx;

           

            m_currChildTask = m_node.GetChild(m_availableChildren[m_currChildNodeIndex].index).CreateTask(m_tree);
            m_currChildTask.SetParent(this);
            m_currChildStatus = m_currChildTask.GetStatus();
            m_currChildTask.Terminated += OnChildTerminated;

            m_scheduler.Schedule(m_currChildTask);

            return true;
        }
        public void OnChildTerminated(ITask child)
        {
            m_currChildStatus = child.GetStatus();
            if (m_currChildStatus != Status.COMPLETED)
            {
                float p = m_availableChildren[m_currChildNodeIndex].prob;
                m_probTotal -= p;
                m_availableChildren.RemoveAt(m_currChildNodeIndex);


            }
            m_scheduler.Resume(this);
        }
    }
}
