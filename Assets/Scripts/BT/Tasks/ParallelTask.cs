﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BT
{
    public class ParallelTask : AbstractTask
	{
        protected Parallel m_node;
        protected List<ITask> m_childTasks;

        protected int m_failNum;
        protected int m_successNum;
        protected Status m_terminateStat;
        protected ITree m_tree;

        public ParallelTask(ITree bt,Parallel skel_node)
        {
            m_status = Status.NONE;
            m_node = skel_node;
            m_childTasks = new List<ITask>();
            m_terminateStat = Status.NONE;
            m_tree = bt;
        }
        public override void Accept(ITaskVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Start()
        {
            Debug.Assert(m_status == Status.RUNNING, "Start task in not a RUNNING status");

            m_successNum = m_node.SuccessNum;
            m_failNum = m_node.FailNum;
            _Started(this);
        }

        public override Status Tick()
        {
            Debug.Assert(m_status == Status.RUNNING, "ParallelTask::Tick in status" + m_status);

            if (m_childTasks.Count == 0)
            {
                InitChildren();
                m_scheduler.Suspend(this);
                return m_status;
            }
            Debug.Assert(IsCompleted(m_terminateStat), "ParallelTask::Tick is ticked but not finished" + m_status);
            m_status = m_terminateStat;

            return m_status;
        }

        public override void Terminate()
        {
            _Terminated(this);
            CleanChildren();
  
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }

        public void OnChildTerminated(ITask child)
        {
            Status s = UpdateStatus();

            if (IsCompleted(s))
            {
                m_terminateStat = s;
                m_scheduler.Resume(this);
            }
        }

        private void InitChildren()
        {
            int child_num = m_node.ChildCount();
            for (int i = child_num - 1; i >= 0; --i)
            {
                ITask child = m_node.GetChild(i).CreateTask(m_tree);
                child.SetParent(this);
                child.Terminated += OnChildTerminated;

                m_childTasks.Add(child);
            }
            for (int i = 0; i < child_num; ++i)
            {
                m_scheduler.Schedule(m_childTasks[i]);
            }
        }
        private void CleanChildren()
        {
            int child_num = m_childTasks.Count;
     
            for (int i = 0; i < child_num; ++i)
            {
                ITask child = m_childTasks[i];
                child.Terminated -= OnChildTerminated;

                if (!IsCompleted(child.GetStatus()))
                {
                    m_scheduler.Halt(child);
                }
            }
        
        }
        private Status UpdateStatus()
        {
            int child_num = m_childTasks.Count;
            int failed = 0;
            int succeeded = 0;

            for (int i = 0; i < child_num; ++i)
            {
                Status childStatus = m_childTasks[i].GetStatus();
                if (childStatus == Status.COMPLETED)
                    succeeded++;
                if (childStatus == Status.FAILED)
                    failed++;
            }
            if (failed >= m_failNum)
                return Status.FAILED;
            if (succeeded >= m_successNum)
                return Status.COMPLETED;

            return Status.SUSPENDED;
        }
        private bool IsCompleted(Status s)
        {
            return (s == Status.FAILED || s == Status.COMPLETED);
        }

     
    }
}
