﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT.Internals
{
    
    public struct ConditionStrategy
    {
        public delegate bool ConditionEvaluer();
        public ConditionEvaluer cond;

        public ConditionStrategy(BBValueType t, Comparer comp, int bbId, IBlackBoard bb, object refVal)
        {
            cond = () => true;
            switch (t)
            {
                case BBValueType.BOOL:
                    {
                        switch (comp)
                        {
                            case Comparer.EQUAL:
                            {
                                bool val = Convert.ToBoolean(refVal);
                                cond = () => bb.GetBool(bbId) == val;
                                break;
                            }
                            case Comparer.NOT_EQUAL:
                            {
                                bool val = Convert.ToBoolean(refVal);
                                cond = () => bb.GetBool(bbId) != val;
                                break;
                            }
                            default:
                            {
                                bool val = Convert.ToBoolean(refVal);
                                cond = () => bb.GetBool(bbId) == val;
                                break;

                            }
                        }
                        break;
                    }
                case BBValueType.FLOAT:
                    {
                        switch (comp)
                        {
                            case Comparer.EQUAL:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) == val;
                                break;
                            }
                            case Comparer.GREATER:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) > val;
                                break;
                            }
                            case Comparer.GREATER_EQUAL:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) >= val;
                                break;
                            }
                            case Comparer.LESS:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) < val;
                                break;
                            }
                            case Comparer.LESS_EQUAL:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) <= val;
                                break;
                            }
                            case Comparer.NOT_EQUAL:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) != val;
                                break;
                            }
                            default:
                            {
                                float val = Convert.ToSingle(refVal);
                                cond = () => bb.GetFloat(bbId) == val;
                                break;
                            }
                        }
                        break;
                    }
                case BBValueType.INT:
                    {
                        switch (comp)
                        {
                            case Comparer.EQUAL:
                            {
                                int val = Convert.ToInt32(refVal);
                                cond = () => bb.GetInt(bbId) == val;
                                break;
                            }
                            case Comparer.GREATER:
                            {
                                int val = Convert.ToInt32(refVal);
                                cond = () => bb.GetInt(bbId) > val;
                                break;
                            }
                            case Comparer.GREATER_EQUAL:
                            {
                                int val = Convert.ToInt32(refVal);
                                cond = () => bb.GetInt(bbId) >= val;
                                break;
                            }
                            case Comparer.LESS:
                            {
                                int val = Convert.ToInt32(refVal);
                                cond = () => bb.GetInt(bbId) < val;
                                break;
                            }
                            case Comparer.LESS_EQUAL:
                            {
                                int val = Convert.ToInt32(refVal);
                                cond = () => bb.GetInt(bbId) <= val;
                                break;
                            }
                            case Comparer.NOT_EQUAL:
                            {
                                int val = Convert.ToInt32(refVal);
                                cond = () => bb.GetInt(bbId) != val;
                                break;
                            }
                            default:
                                {
                                    int val = Convert.ToInt32(refVal);
                                    cond = () => bb.GetInt(bbId) == val;
                                    break;
                                }
                        }
                        break;
                    }
            }
        }
    }
}
