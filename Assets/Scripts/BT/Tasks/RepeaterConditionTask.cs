﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using BT.Internals;
using System;

namespace BT
{
	public class RepeaterConditionTask : RepeaterTask
	{
        protected ConditionStrategy m_cond;

        public RepeaterConditionTask(ITree tree, IRepeater node, IBlackBoard bb, int propId): base(tree,node)
        {
            m_cond = new ConditionStrategy(node.GetComparerType(), node.GetComparer(), propId, bb, node.GetRefValue());
     
        }
        public override Status Tick()
        {
            if (m_childTask != null)
            {
                
                m_childTask.Terminated -= OnChildTerminated;

                bool conditionValid = m_cond.cond();
                if (!conditionValid)
                {
                    m_status = m_childTask.GetStatus();
                    return m_status;
                }
                StartChild();
            }
            else
            {
                bool conditionValid = m_cond.cond();
                if (!conditionValid)
                {
                    m_status = Status.FAILED;
                    return m_status;
                    //return Status.FAILED;
                }
                //first execution
                StartChild();
            }
            m_scheduler.Suspend(this);
            return m_status;
        }
        private void StartChild()
        {
            m_childTask = m_node.GetChild(0).CreateTask(m_tree);
            m_childTask.SetParent(this);
            m_childTask.Terminated += OnChildTerminated;

            m_scheduler.Schedule(m_childTask);

        }
        private void OnChildTerminated(ITask child)
        {
            m_scheduler.Resume(this);
        }
        public override void Terminate()
        {
            _Terminated(this);
            if (m_childTask != null)
            {
                m_childTask.Terminated -= OnChildTerminated;
                CleanChild();
            }
        }

        public override void Start()
        {
            _Started(this);
        }
        private void CleanChild()
        {
            if (!IsCompleted(m_childTask.GetStatus()))
            {
                m_scheduler.Halt(m_childTask);
            }

        }
        private bool IsCompleted(Status s)
        {
            return (s == Status.FAILED || s == Status.COMPLETED);
        }
    }
}
