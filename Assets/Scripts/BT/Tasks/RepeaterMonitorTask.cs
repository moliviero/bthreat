﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using BT.Internals;


namespace BT
{
	public class RepeaterMonitorTask : RepeaterTask
	{
        private ConditionStrategy m_cond;
        private IBlackBoard m_bb;
        private int m_propId;

        public RepeaterMonitorTask(ITree tree, IRepeater node, IBlackBoard bb, int propId)
            : base(tree, node)
        {
            m_bb = bb;
            m_propId = propId;
            m_cond = new ConditionStrategy(node.GetComparerType(), node.GetComparer(), propId, bb, node.GetRefValue());
  
        }


        public override Status Tick()
        {
            if (m_status != Status.RUNNING)
            {
                //halted by invalidated condition
                if (m_childTask != null)
                {
                    m_childTask.Terminated -= OnChildTerminated;
                    m_scheduler.Halt(m_childTask);
                }
                return m_status;
            }
           
            if (m_childTask != null)
            {
                m_childTask.Terminated -= OnChildTerminated;

            }
            StartChild();
           
            m_scheduler.Suspend(this);
            return m_status;
        }
        private void StartChild()
        {
            m_childTask = m_node.GetChild(0).CreateTask(m_tree);
            m_childTask.SetParent(this);
            m_childTask.Terminated += OnChildTerminated;

            m_scheduler.Schedule(m_childTask);

        }
        private void OnChildTerminated(ITask child)
        {
            if(m_status == Status.SUSPENDED)
                m_scheduler.Resume(this);
        }
        public override void Terminate()
        {
            _Terminated(this);
            if (m_childTask != null)
            {
                m_childTask.Terminated -= OnChildTerminated;
                CleanChild();
            }
        }

        public override void Start()
        {
            m_bb.ObserverSubscribe(m_propId, OnPropertyUpdated);
            _Started(this);
        }
        public void OnPropertyUpdated(IBlackBoard bb, int propId)
        {
            bool validCondition = m_cond.cond();

            if (!validCondition)
            {
                m_scheduler.Resume(this, Status.FAILED);
            }
        }

        private void CleanChild()
        {
            if (!IsCompleted(m_childTask.GetStatus()))
            {
                m_scheduler.Halt(m_childTask);
            }


        }
        private bool IsCompleted(Status s)
        {
            return (s == Status.FAILED || s == Status.COMPLETED);
        }

    }
}
