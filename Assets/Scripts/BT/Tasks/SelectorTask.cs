﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Diagnostics;

namespace BT
{
	public class SelectorTask : AbstractTask
	{
        protected INodeComposite m_node;

        protected int m_currChildNodeIndex;
        protected ITask m_currChildTask;
        protected Status m_currChildStatus;
        protected ITree m_tree;

        public SelectorTask(ITree bt, INodeComposite node)
        {
            m_node = node;
            m_currChildStatus = Status.NONE;
            m_currChildNodeIndex = -1;
            m_currChildTask = null;
            m_tree = bt;
        }
        public override void Accept(ITaskVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Start()
        {
            Debug.Assert(m_status == Status.RUNNING, "Start task in not a RUNNING status");

            _Started(this);
        }

        public override Status Tick()
        {
            Debug.Assert(m_status == Status.RUNNING, "SelectorTask::Tick in status" + m_status);

            if (m_currChildStatus != Status.COMPLETED && MoveNext())
            {
                m_scheduler.Suspend(this);
                return m_status;
            }
            m_status = m_currChildStatus;

            return m_status;
        }

        public override void Terminate()
        {
            _Terminated(this);
            CleanChildren();
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
        private void CleanChildren()
        {

            m_currChildTask.Terminated -= OnChildTerminated;

            if (!IsCompleted(m_currChildTask.GetStatus()))
            {
                m_scheduler.Halt(m_currChildTask);
            }

        }
        private bool IsCompleted(Status s)
        {
            return (s == Status.FAILED || s == Status.COMPLETED);
        }

        private bool MoveNext()
        {
            int childCount = m_node.ChildCount();
            if (m_currChildNodeIndex >= childCount)
                return false;

            if (m_currChildTask != null)
            {
                m_currChildTask.Terminated -= OnChildTerminated;
            }

            ++m_currChildNodeIndex;

            if (m_currChildNodeIndex >= childCount)
                return false;

            m_currChildTask = m_node.GetChild(m_currChildNodeIndex).CreateTask(m_tree);
            m_currChildTask.SetParent(this);
            m_currChildStatus = m_currChildTask.GetStatus();
            m_currChildTask.Terminated += OnChildTerminated;

            m_scheduler.Schedule(m_currChildTask);

            return true;
        }
        public void OnChildTerminated(ITask child)
        {
            m_currChildStatus = child.GetStatus();
            m_scheduler.Resume(this);
        }
    }
}
