﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BT.Internals;
using System.Diagnostics;

namespace BT
{
	public class MonitorTask : AbstractTask
	{
        private Monitor m_node;
        protected IBlackBoard m_bb;
        protected int m_bbId;
        protected ConditionStrategy m_cond;

        public MonitorTask(Monitor node, IBlackBoard bb, int propId)
        {
            m_node = node;
            m_bb = bb;
            m_bbId = propId;

            m_cond = new ConditionStrategy(node.GetComparerType(), node.GetComparer(), m_bbId, bb, node.GetRefValue());
        }
        public override void Accept(ITaskVisitor visitor)
        {
            visitor.Visit(this);
        }

        public override void Start()
        {
            m_bb.ObserverSubscribe(m_bbId,OnPropertyUpdated);
            _Started(this);
        }

        public override Status Tick()
        {
            if (m_status == Status.RUNNING)
            {
                bool validCondition = m_cond.cond();

                if (validCondition)
                {
                    m_scheduler.Suspend(this);
                    return m_status;
                }
                m_status = Status.FAILED;
            }
            Debug.Assert(m_status == Status.FAILED, "MonitorTask::Tick Tick should suspend or fail. Current status is " + m_status);

            return m_status;
        }

        public override void Terminate()
        {
            m_bb.ObserverUnsubscribe(m_bbId, OnPropertyUpdated);
            _Terminated(this);
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }

        public void OnPropertyUpdated(IBlackBoard bb, int propId)
        {
            bool validCondition = m_cond.cond();

            if (!validCondition)
            {
                m_scheduler.Resume(this, Status.FAILED);
            }
        }
    }
}
