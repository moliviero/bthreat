﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class ResetVisitor : ITaskVisitor
	{
        public void Visit(SequenceTask t)
        {
       
          //  for(int i =0; i < t.)
        }

        public void Visit(ParallelTask t)
        {
           // t.Reset();
        }


        public void Visit(SelectorTask t)
        {
            throw new NotImplementedException();
        }


        public void Visit(ConditionTask conditionTask)
        {
            throw new NotImplementedException();
        }


        public void Visit(MonitorTask monitorTask)
        {
            throw new NotImplementedException();
        }


        public void Visit(ActionDelegateTask actionTask)
        {
            throw new NotImplementedException();
        }


        public void Visit(ActionSetterTask actionSetterTask)
        {
            throw new NotImplementedException();
        }
    }
}
