﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections;
using UnityEngine;

namespace BT.Unity
{
    public class BTStatus : YieldInstruction
    {
        public readonly Status status;

        public BTStatus(Status s)
        {
            status = s;
        }

    }
  

    public class BTCoroutine
    {
        public event System.Action OnEnd = delegate { };

        private IEnumerator m_coroutine;
        private Status m_stat;
        private IEnumerator m_self;

        public readonly MonoBehaviour m_agent;

        public BTCoroutine(MonoBehaviour agent)
        {
            m_agent = agent;
        }
        public Status GetStatus()
        {
            return m_stat;
        }
        public void Init(IEnumerator routine)
        {
            m_self = Execute(routine);
        }
        public void Start()
        {
            m_agent.StartCoroutine(m_self);
        }
        public void Halt()
        {
            m_agent.StopCoroutine(m_self);
        }
        public IEnumerator Execute(IEnumerator routine)
        {
            m_stat = Status.RUNNING;
            m_coroutine = routine;
            while (true)
            {
                if (!m_coroutine.MoveNext())
                {
                    BTStatus s = m_coroutine.Current as BTStatus;
                    m_stat = s != null ? s.status : Status.COMPLETED;
                    OnEnd();
                    yield break;
                }
                else
                {
                    BTStatus s = m_coroutine.Current as BTStatus;
                    if (s != null)
                    {
                        if (s.status != Status.RUNNING && s.status !=  Status.SUSPENDED)
                        {
                            m_stat = s.status;
                            OnEnd();
                            yield break;
                        }
                    }
                    yield return m_coroutine.Current;
                }

            }
        }

    }
	public static class BTCoroutineUtils
	{
        public static BTCoroutine BTCoroutineStart(this MonoBehaviour agent, IEnumerator coroutine)
        {
            BTCoroutine c = new BTCoroutine(agent);
            c.Init(coroutine);
            c.Start();

            return c;

        }
	}
}
