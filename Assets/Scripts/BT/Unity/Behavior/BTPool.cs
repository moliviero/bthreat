﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using System;

namespace BT.Unity
{
	public class BTPool : MonoBehaviour
	{
        [SerializeField]
        private BTTree[] m_trees;

        private INode[] m_skeletonRoots;

        private INodeFactory f;
        private IBlackBoard m_worldContext;

        public IBlackBoard WorldContext
        {
            get
            {
                return m_worldContext;
            }
        }
        void Awake()
        {
            f = new DefaultNodeFactory();
            m_skeletonRoots = new INode[m_trees.Length];

            for (int i = 0; i < m_skeletonRoots.Length; ++i)
            {
                m_skeletonRoots[i] = BTTree.BuildRecursive(f, null, m_trees[i].INodeRoot);
            }

            m_worldContext = new BlackBoard();
        }

        public ITree InstantiateTree(BTTree prototype, MonoBehaviour agent)
        {
            ITree tree = f.CreateTree();

            tree.SetWorldState(m_worldContext);
            tree.SetContext(new BlackBoard());
            tree.SetScheduler(new Scheduler());

            int index = -1;
            int id = prototype.GetInstanceID();
            for (int i = 0; i < m_trees.Length; ++i)
            {
                if (m_trees[i].GetInstanceID() == id)
                {
                    index = i;
                    break;
                }
            }

            if (index == -1)
            {
                Debug.LogError("BTPool::InstantiateTree requested tree not available in the pool");
            }

            tree.SetRoot(m_skeletonRoots[index]);

            BTTree.BindActions(tree.GetContext(), agent);

            return tree;
        }
       
	}
}
