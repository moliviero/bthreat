﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections;
using UnityEngine;

namespace BT.Unity 
{
    public class BTAgent : MonoBehaviour, IBTAgent
    {
        [SerializeField]
        private BTTree m_BT;

        [SerializeField]
        private bool m_usePool;

        [SerializeField]
        private BTPool m_pool;

        [SerializeField]
        private MonoBehaviour m_agentController;

        [SerializeField]
        private float m_frameTickDelay;

        [SerializeField]
        private bool m_autoStart = false;

        private ITree m_tree;
        private IEnumerator m_updateRoutine;

        void Awake()
        {
            if (m_pool == null && m_usePool)
                m_pool = GameObject.FindObjectOfType<BTPool>();

            if (m_usePool)
            {
                if (m_pool != null)
                    m_tree = m_pool.InstantiateTree(m_BT, m_agentController);
                else
                {
                    Debug.LogError("BTAgent requires BTPool but none is referenced nor available in the scene");
                    enabled = false;
                }
            }
            else
            {
                m_tree = BTUnityUtils.InstantiateTree(m_BT, m_agentController);
            }
            
        }

        void Start()
        {
            if (m_autoStart)
                TickStart();
        }
        public void ResetTree()
        {
            StopCoroutine(m_updateRoutine);
            m_tree.Reset();
        }
        public void TickStart()
        {
            m_tree.Start();
            m_updateRoutine = TickRoutine();
            StartCoroutine(m_updateRoutine);
        }
        public ITree GetTree()
        {
            return m_tree;
        }

        private IEnumerator TickRoutine()
        {
            
            while (true)
            {
                int index = 0;
                while (index < m_frameTickDelay)
                {
                    index++;
                    yield return null;
                }
                m_tree.Tick();
                yield return null;
            }
        }


        public IBlackBoard GetContext()
        {
            return m_tree.GetContext();
        }
        public IBlackBoard GetWorldState()
        {
            return m_tree.GetWorldState();
        }
    }
}
