﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT.Unity
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class BTActionClassAttribute : Attribute
    { }
 
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class BTActionAttribute : Attribute
    { }
    
	public class ActionStartAttribute : BTActionAttribute
	{
        public readonly string actionName;
        public ActionStartAttribute(string action)
        {
            actionName = action;
        }
	}
    public class ActionTickAttribute : BTActionAttribute
    {
        public readonly string actionName;
        public ActionTickAttribute(string action)
        {
            actionName = action;
        }
    }
    public class ActionTerminateAttribute : BTActionAttribute
    {
        public readonly string actionName;
        public ActionTerminateAttribute(string action)
        {
            actionName = action;
        }
    }

    //------------ courotine attributes --------------------------
    //public class ActionRoutineStartAttribute : BTActionAttribute
    //{
    //    public readonly string actionName;
    //    public ActionRoutineStartAttribute(string action)
    //    {
    //        actionName = action;
    //    }
    //}
    public class ActionRoutineTickAttribute : BTActionAttribute
    {
        public readonly string actionName;
        public ActionRoutineTickAttribute(string action)
        {
            actionName = action;
        }
    }
    //public class ActionRoutineTerminateAttribute : BTActionAttribute
    //{
    //    public readonly string actionName;
    //    public ActionRoutineTerminateAttribute(string action)
    //    {
    //        actionName = action;
    //    }
    //}
}
