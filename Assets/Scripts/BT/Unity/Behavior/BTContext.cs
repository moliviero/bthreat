﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using BT;
using System.Collections.Generic;
using System.Reflection;
using System;

namespace BT.Unity
{
 	public class BTContext : MonoBehaviour
	{
        private IBlackBoard m_bb;

        void Awake()
        {
            m_bb = new BlackBoard();

            BindStaticDelegates();
        }
        static IEnumerable<Type> GetTypesWithAttribute(Assembly assembly, Type attrType)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsClass && type.GetCustomAttributes(attrType, false).Length > 0)
                {
                    yield return type;
                }
            }
        }
        private void BindStaticDelegates()
        {
            Assembly asm = GetType().Assembly;
          
            foreach(Type classWithAttr in GetTypesWithAttribute(asm,typeof(BTActionClassAttribute)))
            {
                MethodInfo[] methods = classWithAttr.GetMethods(BindingFlags.Static);

                foreach (MethodInfo m in methods)
                {
                    foreach (Attribute att in Attribute.GetCustomAttributes(m, typeof(BTActionAttribute)))
                    {

                        if (att.GetType() == typeof(ActionStartAttribute))
                        {
                            ActionStartAttribute a = att as ActionStartAttribute;
                            string methodName = a.actionName + Action.ACTION_START_SUFFIX;
                            TaskStartAction startDelegate = Delegate.CreateDelegate(typeof(TaskStartAction), m) as TaskStartAction;
                            int id = BTUtils.PropertyToId(methodName);
                            m_bb.SetObject<TaskStartAction>(id, startDelegate);
                        }
                        else if (att.GetType() == typeof(ActionTickAttribute))
                        {
                            ActionTickAttribute a = att as ActionTickAttribute;
                            string methodName = a.actionName + Action.ACTION_TICK_SUFFIX;
                            TaskTickAction tickDelegate = Delegate.CreateDelegate(typeof(TaskTickAction), m) as TaskTickAction;
                            int id = BTUtils.PropertyToId(methodName);
                            m_bb.SetObject<TaskTickAction>(id, tickDelegate);

                        }
                        else if (att.GetType() == typeof(ActionTerminateAttribute))
                        {
                            ActionTerminateAttribute a = att as ActionTerminateAttribute;
                            string methodName = a.actionName + Action.ACTION_TERMINATE_SUFFIX;
                            TaskTerminateAction terminateDelegate = Delegate.CreateDelegate(typeof(TaskTerminateAction),m) as TaskTerminateAction;
                            int id = BTUtils.PropertyToId(methodName);
                            m_bb.SetObject<TaskTerminateAction>(id, terminateDelegate);
                        }
                        //else if (att.GetType() == typeof(ActionRoutineStartAttribute))
                        //{
                        //    ActionRoutineStartAttribute a = att as ActionRoutineStartAttribute;
                        //    string methodName = a.actionName + Action.ACTION_START_SUFFIX;
                        //    CoroutineTaskStartAction terminateDelegate = Delegate.CreateDelegate(typeof(CoroutineTaskStartAction),m) as CoroutineTaskStartAction;
                        //    int id = BTUtils.PropertyToId(methodName);
                        //    m_bb.SetObject<CoroutineTaskStartAction>(id, terminateDelegate);
                        //}
                        else if (att.GetType() == typeof(ActionRoutineTickAttribute))
                        {
                            ActionRoutineTickAttribute a = att as ActionRoutineTickAttribute;
                            string methodName = a.actionName + Action.ACTION_TICK_SUFFIX;
                            CoroutineTaskTickAction terminateDelegate = Delegate.CreateDelegate(typeof(CoroutineTaskTickAction),m) as CoroutineTaskTickAction;
                            int id = BTUtils.PropertyToId(methodName);
                            m_bb.SetObject<CoroutineTaskTickAction>(id, terminateDelegate);
                        }
                        //else if (att.GetType() == typeof(ActionRoutineTerminateAttribute))
                        //{
                        //    ActionRoutineTerminateAttribute a = att as ActionRoutineTerminateAttribute;
                        //    string methodName = a.actionName + Action.ACTION_TERMINATE_SUFFIX;
                        //    CoroutineTaskTerminateAction terminateDelegate = Delegate.CreateDelegate(typeof(CoroutineTaskTerminateAction),m) as CoroutineTaskTerminateAction;
                        //    int id = BTUtils.PropertyToId(methodName);
                        //    m_bb.SetObject<CoroutineTaskTerminateAction>(id, terminateDelegate);
                        //}
                    }
                }
            }
            
        }
        public IBlackBoard Context
        {
            get
            {
                return m_bb;
            }
        }
	}
}
