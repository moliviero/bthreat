﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using System;

namespace BT.Unity
{
	public class LogTask : CustomTickableTask
	{
        private string m_msg;
        private INode m_node;
        public LogTask(INode node, string msg)
        {
            m_msg = msg;
            m_node = node;
        }
        public override void Accept(ITaskVisitor visitor)
        {
            throw new NotImplementedException();
        }

        public override void Start()
        {
            _Started(this);
        }

        public override Status Tick()
        {
            Debug.Log(m_msg);
            _Updated(this);
            SetStatus(Status.COMPLETED);
            return m_status;

        }

        public override void Terminate()
        {
            _Terminated(this);
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
    }
}
