﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;


namespace BT.Unity
{
    public class CoroutineContextActionTask : AbstractTask
	{
        private event TaskStartAction OnStart;
        private event CoroutineTaskTickAction OnTick;
        private event TaskTerminateAction OnTerminate;

        private CoroutineContextAction m_node;
        private IBlackBoard m_bb;
        private BTCoroutine m_coroutine;
        private MonoBehaviour m_agent;

        public CoroutineContextActionTask(IBlackBoard bb, CoroutineContextAction node)
        {
            m_node = node;
            m_bb = bb;
        }
        public override void Accept(ITaskVisitor visitor)
        {
            throw new NotImplementedException();
        }

        public override void Start()
        {
            string actName = m_node.GetActionName();
            int id_start = m_bb.PropertyToId(actName + Action.ACTION_START_SUFFIX);
            int id_tick = m_bb.PropertyToId(actName + Action.ACTION_TICK_SUFFIX);
            int id_terminate = m_bb.PropertyToId(actName + Action.ACTION_TERMINATE_SUFFIX);

            TaskStartAction startDelegate = m_bb.GetObject<TaskStartAction>(id_start);
            CoroutineTaskTickAction tickDelegate = m_bb.GetObject<CoroutineTaskTickAction>(id_tick);
            TaskTerminateAction terminateDelegate = m_bb.GetObject<TaskTerminateAction>(id_terminate);

            if (startDelegate != null)
                OnStart += startDelegate;
            else
                OnStart += delegate { };

            if (terminateDelegate != null)
                OnTerminate += terminateDelegate;
            else
                OnTerminate += delegate { };

            if (tickDelegate != null)
                OnTick += tickDelegate;
            else
                OnTick += _onTickDefault;

            m_agent = m_bb.GetObject<MonoBehaviour>(BTUtils.PropertyToId(BTDefs.AGENT_KEY));
            _Started(this);
        }

        private IEnumerator _onTickDefault()
        {
            yield return Status.COMPLETED;
        }
        public override Status Tick()
        { 
            if (m_coroutine == null)
            {
                m_coroutine = m_agent.BTCoroutineStart(OnTick());
                m_coroutine.OnEnd += OnCoroutineEnded;
            }
            Status actStatus = m_coroutine.GetStatus();
            if (!IsActive(actStatus))
            {
                m_status = actStatus;
            }
            else
            {
                m_scheduler.Suspend(this);
            }
            return m_status;
        }

        public override void Terminate()
        {
            if (m_coroutine != null)
            {
                m_coroutine.OnEnd -= OnCoroutineEnded;



                if (IsActive(m_coroutine.GetStatus()))
                {
                    m_coroutine.Halt();
                }
            }
            _Terminated(this);
            OnTerminate();
           
        }

        public override INode SkeletonNode()
        {
            return m_node;
        }
        private bool IsActive(Status s)
        {
            return s == Status.RUNNING || s == Status.SUSPENDED;
        }

        private void OnCoroutineEnded()
        {
            m_status = m_coroutine.GetStatus();
            m_scheduler.Resume(this, m_status);
        }
    }
}
