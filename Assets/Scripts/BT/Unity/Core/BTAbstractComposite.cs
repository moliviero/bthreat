﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Unity
{
    public abstract class BTAbstractComposite : BTAbstractNode
	{
        [SerializeField]
        [HideInInspector]
        protected List<BTAbstractNode> m_children;

        public virtual int MaxChildCount { get { return int.MaxValue; } }
        public List<BTAbstractNode> Children
        {
            get
            {
                return m_children;
            }
            set
            {
                m_children = value;
            }
        }
        public override IEnumerator<BTAbstractNode> GetEnumerator()
        {
            yield return this;
            if (m_children != null)
            {
                IEnumerator<BTAbstractNode> it = m_children.GetEnumerator();
                while (it.MoveNext())
                {
                    yield return it.Current;
                }
            }
            yield  break;// return  Enumerable.Empty<IBTComponent>();
        }

        public virtual void AddChild(BTAbstractNode child)
        {
            if (m_children == null)
                m_children = new List<BTAbstractNode>();
            m_children.Add(child);
        }
        public virtual BTAbstractNode GetChild(int childNum)
        {
            if (m_children == null)
                return null;
            return m_children[childNum];
        }
        public virtual int GetChildNum()
        {
            if (m_children == null)
                return 0;
            return m_children.Count;
            
        }
        public virtual void RemoveChild(BTAbstractNode node)
        {
            if (m_children != null)
                m_children.Remove(node);
        }
        public virtual void DetachChild(BTAbstractNode node)
        {
            if (m_children != null)
            {
                int index = m_children.IndexOf(node);
                m_children[index] = null;
                node.SetParent(null);
            }
        }
        public virtual void DetachChild(int index)
        {
            if (m_children != null)
            {
                BTAbstractNode node = m_children[index];
                m_children[index] = null;
                node.SetParent(null);
            }
        }
        public virtual void SetChild(BTAbstractNode node, int index)
        {
            if (m_children != null)
            {
                m_children[index] = node;
            }
        }
        public virtual void RemoveChildAt(int index)
        {
            if (m_children != null)
                m_children.RemoveAt(index);
        }


        
	}
}
