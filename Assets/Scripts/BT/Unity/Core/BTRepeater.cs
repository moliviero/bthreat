﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using UnityEngine;

namespace BT.Unity
{
	public class BTRepeater  : BTAbstractComposite, IConditionHolder
	{
        public const float m_minSize = 50f;
        public const float m_childW = 32f;

        [SerializeField]
        private Repeater.Type m_type;
        public Repeater.Type ConditionType
        {
            get
            {
                return m_type;
            }
        }

        [SerializeField]
        [Condition]
        private BTConditionHolder m_conditionHolder;

        public BTConditionHolder Condition
        {
            get { return m_conditionHolder; }
        }
        [SerializeField]
        private int m_repeatNum;

        public int RepeatNumber
        {
            get
            {
                return m_repeatNum;
            }
        }
        public override int MaxChildCount { get { return 1; } }
        public void Init()
        {
            m_pos = new Rect(0, 0, m_minSize, m_minSize);

            if (m_children == null)
            {
                m_children = new List<BTAbstractNode>();
                m_children.Add(null);
            }
        }
	}
}
