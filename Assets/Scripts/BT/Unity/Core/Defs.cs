﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using System;

namespace BT.Unity
{
    public enum BTActionType { MANUAL, DELEGATE, COROUTINE}
    public enum BTEditorEvent { ATTACH_CHILD_BEGIN, ATTACH_CHILD_END, CHILD_OP_ERR}
    public delegate void BTEventHandler(BTAbstractNode source, BTEditorEvent e, params object[] args);

    public static class BTDefs
    {
        public static BTStatus COMPLETED = new BTStatus(Status.COMPLETED);
        public static BTStatus RUNNING = new BTStatus(Status.RUNNING);
        public static BTStatus ABORTED = new BTStatus(Status.ABORTED);
        public static BTStatus FAILED = new BTStatus(Status.FAILED);
        public static BTStatus SUSPENDED = new BTStatus(Status.SUSPENDED);

        public const string AGENT_KEY = "_BTAgent";
    }

    [Serializable]
    public struct BTConditionHolder
    {
        [SerializeField]
        public string valueName;
        [SerializeField]
        public BT.BBValueType valueType;
        [SerializeField]
        public BT.Comparer comparer;
     
        [SerializeField]
        public float floatVal;
        [SerializeField]
        public int intVal;
        [SerializeField]
        public bool boolVal;

        [SerializeField]
        public BBLocation watch;

        public object GetRefValue()
        {
            object ret = null;
            switch (valueType)
            {
                case BT.BBValueType.BOOL:
                    {
                        ret = boolVal;
                        break;
                    }
                case BT.BBValueType.FLOAT:
                    {
                        ret = floatVal;
                        break;
                    }
                case BT.BBValueType.INT:
                    {
                        ret = intVal;
                        break;
                    }
            }
            return ret;
        }
    }
}
