﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;

namespace BT.Unity
{
	public class BTActionSetter: BTAbstractNode
	{
        public const float m_minSize = 50f;
        public const float m_childW = 32f;

        [SerializeField]
        private string valueName;
        public string ValueName { get { return valueName; } }
        [SerializeField]
        private BT.BBValueType valueType;
        public BT.BBValueType ValueType { get { return valueType; } }
    
        [SerializeField]
        private BT.BBLocation blackBoardSource;
        public BT.BBLocation BlackBoardSource { get { return blackBoardSource; } }

        [SerializeField]
        private float floatVal;
        [SerializeField]
        private int intVal;
        [SerializeField]
        private bool boolVal;

        public object GetRefValue()
        {
            object ret = null;
            switch (valueType)
            {
                case BT.BBValueType.BOOL:
                {
                    ret = boolVal;
                    break;
                }
                case BT.BBValueType.FLOAT:
                {
                    ret = floatVal;
                    break;
                }
                case BT.BBValueType.INT:
                {
                    ret = intVal;
                    break;
                }
            }
            return ret;
        }
        public void Init()
        {
            m_pos = new Rect(0, 0, m_minSize, m_minSize);
        }
	}
	
	
}
