﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Unity
{
    
    public abstract class BTAbstractNode : ScriptableObject, IEnumerable<BTAbstractNode>
	{
        [SerializeField]
        [HideInInspector] 
        private string description;
        public string Description { get { return description; } }

        [SerializeField]
        [HideInInspector]
        private string m_detailedDescription;
        
        [SerializeField]
        [HideInInspector]
        protected Rect m_pos;

        [SerializeField]
        [HideInInspector]
        protected BTAbstractComposite m_parent;

        private int m_trackedNodeId;
        public int TrackedNodeId
        {
            get
            {
                return m_trackedNodeId;
            }
            set
            {
                m_trackedNodeId = value;
            }
        }
        public void SetPosition(Vector2 pos)
        {
            m_pos.x = pos.x;
            m_pos.y = pos.y;
        }
        public Vector2 GetPosition()
        {
            return new Vector2(m_pos.x, m_pos.y);
        }
        public Rect GetBounds()
        {
            return m_pos;
        }
        public void SetBounds(Rect b)
        {
            m_pos = b;
        }
        public void SetWidth(float w)
        {
          
            m_pos.width = w;
        }
        public float GetWidth()
        {
            return m_pos.width;
        }

        public virtual IEnumerator<BTAbstractNode> GetEnumerator()
        {
            yield return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            yield return this;
        }

      //  public abstract void Draw(int handle, BTEventHandler handler);

        public void SetParent(BTAbstractComposite parent)
        {
            m_parent = parent;   
        }
        public BTAbstractComposite GetParent()
        {
            return m_parent;
        }
        public bool HasParent()
        { 
            return m_parent != null;
        }

     

    }
}
