﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using BT.Unity;
using System.Reflection;
using System.Diagnostics;

namespace BT.Unity
{
	public class BTTree : ScriptableObject
	{
        [SerializeField]
        private List<BTAbstractNode> m_nodes;
        public List<BTAbstractNode> Nodes 
        { 
            get 
            {
                if (m_nodes == null)
                    m_nodes = new List<BTAbstractNode>();
                return m_nodes; 
            } 
        }

        [SerializeField]
        private BTRoot m_root;

#if UNITY_EDITOR
        public void Clear(bool includeRoot = false)
        {
            BTRoot root = m_root;

            if (includeRoot)
            {
                while (m_nodes.Count > 0)
                {
                    BTAbstractNode node = m_nodes[0];
                    m_nodes.RemoveAt(0);
                    GameObject.DestroyImmediate(node, true);
                }

            }
            else
            {
                while (m_nodes.Count > 0)
                {
                    BTAbstractNode node = m_nodes[0];
                    m_nodes.RemoveAt(0);
                    if(node != m_root)
                        GameObject.DestroyImmediate(node, true);
                }
                AddNode(m_root);

            }
                
        }
#endif
        public void Init()
        {
            m_nodes = new List<BTAbstractNode>();
        }

        public void AddNode(BTAbstractNode node)
        {
            m_nodes.Add(node); 
        }

        public BTRoot Root { get { return m_root; }set { m_root = value; } }

        public void RemoveNode(BTAbstractNode node)
        {
            m_nodes.Remove(node);
        }

        public ITree InstantiateTreeSkeleton(MonoBehaviour agent)
        {
            INodeFactory f = new DefaultNodeFactory();

            ITree tree = f.CreateTree();
            tree.SetContext(new BlackBoard());
            tree.SetScheduler(new Scheduler());
            BTAbstractNode n =  m_root.GetChild(0);

            INode root = BuildRecursive(f, null, n);

            tree.SetRoot(root);

            BindActions(tree.GetContext(), agent);

            return tree;

        }
        public BTAbstractNode INodeRoot
        {
            get
            {
                return m_root.GetChild(0);
            }
        }
        /*! \brief Insert agents methods into specified blackboard.
         *        
         *
         *  Detailed description starts here.
         */
        public static void BindActions(IBlackBoard context, MonoBehaviour agent)
        {
            if (agent == null)
                return;
          //  IBlackBoard context = tree.GetContext();
            System.Diagnostics.Debug.Assert(context != null , "BTTree::BindActions: trying to bind agent with a tree without context");

            context.SetObject<MonoBehaviour>(BTUtils.PropertyToId(BTDefs.AGENT_KEY), agent);
            MethodInfo[] methods = agent.GetType().GetMethods();
            foreach(MethodInfo m in methods)
            {
                foreach (Attribute att in Attribute.GetCustomAttributes(m, typeof(BTActionAttribute)))
                {
                
                    if(att.GetType() == typeof(ActionStartAttribute))
                    {
                        ActionStartAttribute a = att as ActionStartAttribute;
                        string methodName = a.actionName + Action.ACTION_START_SUFFIX;
                        TaskStartAction startDelegate = Delegate.CreateDelegate(typeof(TaskStartAction), agent, m.Name) as TaskStartAction;
                        int id = context.PropertyToId(methodName);
                        context.SetObject<TaskStartAction>(id, startDelegate);
                    }
                    else if(att.GetType() == typeof(ActionTickAttribute))
                    {
                        ActionTickAttribute a = att as ActionTickAttribute;
                        string methodName = a.actionName + Action.ACTION_TICK_SUFFIX;
                        TaskTickAction tickDelegate = Delegate.CreateDelegate(typeof(TaskTickAction), agent, m.Name) as TaskTickAction;
                        int id = context.PropertyToId(methodName);
                        context.SetObject<TaskTickAction>(id, tickDelegate);
                    
                    }
                    else if(att.GetType() == typeof(ActionTerminateAttribute))
                    {
                        ActionTerminateAttribute a = att as ActionTerminateAttribute;
                        string methodName = a.actionName + Action.ACTION_TERMINATE_SUFFIX;
                        TaskTerminateAction terminateDelegate = Delegate.CreateDelegate(typeof(TaskTerminateAction), agent, m.Name) as TaskTerminateAction;
                        int id = context.PropertyToId(methodName);
                        context.SetObject<TaskTerminateAction>(id, terminateDelegate);
                    }
                    //else if (att.GetType() == typeof(ActionRoutineStartAttribute)) 
                    //{
                    //    ActionRoutineStartAttribute a = att as ActionRoutineStartAttribute;
                    //    string methodName = a.actionName + Action.ACTION_START_SUFFIX;
                    //    CoroutineTaskStartAction terminateDelegate = Delegate.CreateDelegate(typeof(CoroutineTaskStartAction), agent, m.Name) as CoroutineTaskStartAction;
                    //   // UnityEngine.Debug.Log("methodName " + methodName+ " null? " + (terminateDelegate ==  null) );
                    //    int id = context.PropertyToId(methodName);
                    //    context.SetObject<CoroutineTaskStartAction>(id, terminateDelegate);
                    //}
                    else if (att.GetType() == typeof(ActionRoutineTickAttribute))
                    {
                        ActionRoutineTickAttribute a = att as ActionRoutineTickAttribute;
                        string methodName = a.actionName + Action.ACTION_TICK_SUFFIX;
                        CoroutineTaskTickAction terminateDelegate = Delegate.CreateDelegate(typeof(CoroutineTaskTickAction), agent, m.Name) as CoroutineTaskTickAction;
                        int id = context.PropertyToId(methodName);
                        context.SetObject<CoroutineTaskTickAction>(id, terminateDelegate);
                    }
                    //else if (att.GetType() == typeof(ActionRoutineTerminateAttribute))
                    //{
                    //    ActionRoutineTerminateAttribute a = att as ActionRoutineTerminateAttribute;
                    //    string methodName = a.actionName + Action.ACTION_TERMINATE_SUFFIX;
                    //    CoroutineTaskTerminateAction terminateDelegate = Delegate.CreateDelegate(typeof(CoroutineTaskTerminateAction), agent, m.Name) as CoroutineTaskTerminateAction;
                    //    int id = context.PropertyToId(methodName);
                    //    context.SetObject<CoroutineTaskTerminateAction>(id, terminateDelegate);
                    //}
                }
            }
        }
        public static INode BuildRecursive(INodeFactory f, INodeComposite parent, BTAbstractNode node)
        {
            INode ret = null;

            if (node is BTSequence)
            {
                ISequence seq = f.CreateSequence();
                ret = seq;
            }
            else if (node is BTParallel)
            {
                BTParallel btPar = node as BTParallel;
                IParallel par = f.CreateParallel(btPar.SuccessNum, btPar.FailNum);
                ret = par;
            }
            else if (node is BTSelector)
            {
                ret = f.CreateSelector();
            }
            else if (node is BTCondition)
            {
                BTCondition btCond = node as BTCondition;
                BTConditionHolder ch = btCond.Condition;
                ret = f.CreateCondition(ch.valueName, ch.watch, ch.valueType, ch.comparer, ch.GetRefValue());
            }
            else if (node is BTMonitor)
            {
                BTMonitor btMon = node as BTMonitor;
                BTConditionHolder ch = btMon.Condition;
                ret = f.CreateMonitor(ch.valueName, ch.watch, ch.valueType, ch.comparer, ch.GetRefValue());
            }
            else if (node is BTAction)
            {
                BTAction btACt = node as BTAction;
                ret = f.CreateAction(btACt.ActionName,btACt.ActionType);
            }
            else if (node is BTActionSetter)
            {
                BTActionSetter btSet = node as BTActionSetter;
                ret = f.CreateActionSetter(btSet.ValueName, btSet.BlackBoardSource,btSet.ValueType,btSet.GetRefValue());
            }
            else if (node is BTLog)
            {
                BTLog btLog = node as BTLog;
                ret = new LogAction(btLog.Message);
            }
            else if (node is BTInverter)
            {
                ret = f.CreateInverter();
            }
            else if (node is BTRepeater)
            { 
                BTRepeater rep = node as BTRepeater;
                BTConditionHolder ch = rep.Condition;

                ret = f.CreateRepeater(rep.ConditionType, ch.valueName, ch.watch, ch.valueType, ch.comparer, ch.GetRefValue(), rep.RepeatNumber);
            }
            else if (node is BTRandomSelector)
            {
                BTRandomSelector rs = node as BTRandomSelector;

                ret = f.CreateRandomSelector(rs.Probabilities);
            }
            else if (node is BTReference)
            {
                BTReference r = node as BTReference;
                INode retRef = BuildRecursive(f, parent, r.TreeReference.Root.GetChild(0));
                return retRef;
            }
            

            if (parent != null)
                parent.AppendChild(ret);

            if (node is BTAbstractComposite)
            {
                BTAbstractComposite compNode = node as BTAbstractComposite;
              
                for (int i = 0; i < compNode.GetChildNum(); ++i)
                {
                    BTAbstractNode btChild = compNode.GetChild(i);
                    BuildRecursive(f, ret as INodeComposite, btChild);
                }

            }
            node.TrackedNodeId = ret.GetId();
           
            return ret;
          
        }
        
	}
}
