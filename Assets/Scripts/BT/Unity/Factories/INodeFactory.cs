﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;


namespace BT.Unity
{
	public interface INodeFactory
	{
        IParallel CreateParallel(int successNum, int failNum);
        ICondition CreateCondition(string name, BBLocation bbSource, BBValueType type, Comparer comp, object refVal);
        IMonitor CreateMonitor(string propId, BBLocation watch, BBValueType type, Comparer comp, object refVal);
        ISelector CreateSelector();
        ISelector CreateRandomSelector(params float[] probs);
        ISequence CreateSequence();

        ITree CreateTree();

        IAction CreateAction(string actionName, BTActionType type);

        IAction CreateActionSetter(string valueName, BBLocation bbSource, BBValueType type, object val);
        IInverter CreateInverter();

        IRepeater CreateRepeater(Repeater.Type repType, string propId, BBLocation watch, BBValueType type, Comparer comp, object refVal, int repNum);
    }
}
