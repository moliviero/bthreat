﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT.Unity
{
	public class DefaultNodeFactory : INodeFactory
	{
        public IParallel CreateParallel(int successNum, int failNum)
        {
            return new Parallel(failNum, successNum);
        }

        public ICondition CreateCondition(string name, BBLocation bbSource, BBValueType type, Comparer comp, object refVal)
        {
            return new Condition(name, bbSource, type,comp,refVal);
        }

        public IMonitor CreateMonitor(string propId, BBLocation watch, BBValueType type, Comparer comp, object refVal )
        {
            return new Monitor(propId, watch, refVal, type, comp);
        }

        public ISelector CreateSelector()
        {
            return new Selector();
        }
        public ISelector CreateRandomSelector(params float[] probs)
        {
            return new RandomSelector(probs);
        }
        public ISequence CreateSequence()
        {
            return new Sequence();
        }


        public ITree CreateTree()
        {
            return new Tree();
        }


        public IAction CreateAction(string actionName, BTActionType type)
        {
            IAction act = null;
            
            if (type == BTActionType.COROUTINE)
                act = new CoroutineContextAction(actionName);
            else if (type == BTActionType.DELEGATE)
                act = new Action(actionName);

            return act;
        }



        public IAction CreateActionSetter(string valueName, BBLocation bbSource, BBValueType type, object val)
        {
            return new ActionSetter(valueName, bbSource, type, val);
        }


        public IInverter CreateInverter()
        {
            return new Inverter();
        }


        public IRepeater CreateRepeater(Repeater.Type repType, string propId, BBLocation watch, BBValueType type, Comparer comp, object refVal, int repNum)
        {
            IRepeater ret = null;
            if (repType == Repeater.Type.CONST)
            {
                ret = new Repeater(repNum);
            }
            else 
            {
                ret = new Repeater(propId, repType, watch, type, comp, refVal);
            }

            return ret;
        }
    }
}
