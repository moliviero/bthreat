﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;

namespace BT.Unity
{
    public class CustomTickableAction : AbstractNode, IAction
    {
        private string m_actionName;
        private BTTaskCreator m_factoryMethod;

        public CustomTickableAction(string name, BTTaskCreator factoryMethod)
        {
            m_actionName = name;
            m_factoryMethod = factoryMethod;
        }
        public string GetActionName()
        {
            return m_actionName;
        }
        public ITask CreateTask(ITree bt)
        {
            IBTTask task = m_factoryMethod(this);

            task.SetWorldContext(bt.GetWorldState());
            task.SetLocalContext(bt.GetContext());

            return task;
            //return new CoroutineActionTask(bt.GetContext(), this);
        }
    }
}