﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEditor.AnimatedValues;
using EditorExtension;

namespace BT.Unity.Editor
{
	public static class BTEditorUtils
	{
        public enum Side {TOP, LEFT, RIGHT, DOWN}

        public static void DrawHowToInspector(string desc, GUIStyle style)
        {
            EditorGUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("How To");
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField(desc, style);
            EditorGUILayout.EndVertical();
        }
        public static void DrawHowToInspector(string desc, GUIStyle style, AnimBool show)
        {
            show.target = EditorGUILayout.ToggleLeft("Show Description", show.target);
            if (EditorGUILayout.BeginFadeGroup(show.faded))
            {

                EditorGUILayout.BeginVertical();
                GUILayout.FlexibleSpace();
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                EditorGUILayout.LabelField("How To");
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField(desc, style);
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndFadeGroup();
        }
        public static void DrawDescription(BTEditorWindow editor, BTAbstractNode node, Rect b)
        {
            Rect descBounds = GUILayoutUtility.GetRect(new GUIContent(node.Description), "label");
            descBounds.x = b.xMax + 8f;
            descBounds.y = b.y;
            GUI.Label(descBounds, node.Description);

        }
        public static void DrawStatus(BTEditorWindow editor, Status s, Rect b)
        {
        //    Color c = Color.white;
            GUIStyle descStyle = new GUIStyle();
            descStyle.normal.textColor = Color.white;
            switch (s)
            {
                case Status.COMPLETED:
                {
                    descStyle.normal.textColor = Color.green;
                    break;
                }
                case Status.SUSPENDED:
                {
                    descStyle.normal.textColor = Color.yellow;
                    break;
                }
                case Status.ABORTED:
                {
                    descStyle.normal.textColor = Color.magenta;
                    break;
                }
                case Status.FAILED:
                {
                    descStyle.normal.textColor = Color.red;
                    break;
                }
                case Status.RUNNING:
                {
                    descStyle.normal.textColor = Color.cyan;
                    break;
                }
            }
            GUIContent gc = new GUIContent("" + s);
            
            Rect descBounds = GUILayoutUtility.GetRect(gc, "label");
            descBounds.x = b.xMin -64f; //descBounds.width;
            descBounds.y = b.y;
            GUI.Label(descBounds, gc,descStyle);

        }
        public static void DrawSlotWire(Rect source, Side sourceSide, int sourceSlotIndex, int srcSlotNum,
            Rect target, Side targetSide, int targetSlotIndex, int targetSlotNum, float size = 24f)
        {
            Vector3 sourcePos = Vector3.zero;
            Vector3 targetPos = Vector3.zero;
            Vector3 sourceTan = Vector3.zero;
            Vector3 targetTan = Vector3.zero;

            switch (sourceSide)
            {
                case Side.TOP:
                    {
                        float step = source.width / ((float)srcSlotNum + 1f);
                        sourcePos = new Vector3(target.xMin  + step * sourceSlotIndex, source.yMin - size, 0f);
                        sourceTan = sourcePos - Vector3.up * 50f;
                        break;
                    }
                case Side.DOWN:
                    {
                        float step = source.width / ((float)srcSlotNum + 1f);
                        sourcePos = new Vector3(source.xMin  + step * (sourceSlotIndex + 1), source.yMax + size, 0f);
                        sourceTan = sourcePos + Vector3.up * 50f;
                        break;
                    }
                case Side.RIGHT:
                    {
                        break;
                    }
                case Side.LEFT:
                    {
                        break;
                    }
            }
            switch (targetSide)
            {
                case Side.TOP:
                    {
                        float step = target.width / ((float)targetSlotNum + 1f);
                        targetPos = new Vector3(target.xMin + size * 0.25f  + step * targetSlotIndex, target.yMin - size, 0f);
                        targetTan = targetPos - Vector3.up * 50f;
                        break;
                    }
                case Side.DOWN:
                    {
                        float step = target.width / ((float)targetSlotNum + 1f);
                        targetPos = new Vector3(target.xMin - size * 0.5f + step * targetSlotIndex, target.yMax, 0f);
                        targetTan = targetPos + Vector3.up * 50f;
                        break;
                    }
                case Side.RIGHT:
                    {
                        break;
                    }
                case Side.LEFT:
                    {
                        break;
                    }
            }

            Handles.DrawBezier(sourcePos, targetPos, sourceTan, targetTan, Color.black, null, 2f);
        }
        public static int DrawSlotHandles(Rect nodeBounds, Side s, int num, float size = 24)
        {
            Vector2 start = Vector2.zero;
            Vector2 move = Vector2.zero;

            switch (s)
            {
                case Side.TOP:
                {
                    float step = nodeBounds.width / ((float)num + 1f) ;
                    start = new Vector2(nodeBounds.xMin - size * 0.5f, nodeBounds.yMin - size);
                    move = new Vector2(step,0f);
                    break;
                }
                case Side.DOWN:
                {
                    float step = nodeBounds.width / ((float)num + 1f);
                    start = new Vector2(nodeBounds.xMin - size * 0.5f, nodeBounds.yMax);
                    move = new Vector2(step, 0f);
                    break;
                }
                case Side.RIGHT:
                {
                    float step = nodeBounds.height / ((float)num + 1f);
                    start = new Vector2(nodeBounds.xMax, nodeBounds.yMin - size);
                    move = new Vector2(0f, step);
                  
                    break;
                }
                case Side.LEFT:
                {
                    float step = nodeBounds.height / ((float)num + 1f);
                    start = new Vector2(nodeBounds.xMin - size, nodeBounds.yMin - size);
                    move = new Vector2(0f, step);
        
                    break;
                }
            }

            int index = 0;
            for (int i = 1; i <= num; ++i)
            {
                Rect b = new Rect(start.x + move.x * i, start.y + move.y * i, size, size);
                if (GUI.Button(b, "-"))
                {
                    Event.current.Use();
                    index = i;
                }
            }
            return index - 1;
               
        }
        public static void BTNodeHandleSelection(BTEditorWindow editor, BTAbstractNode node)
        {
            if (Event.current.type == EventType.MouseUp)
            {
              //  Event.current.Use();
                Selection.activeObject = node;
            }
        }
        public static void BTNodeHandleInput(BTEditorWindow editor,BTAbstractNode node)
        {
            
            if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.D && node.HasParent())
            {
                Event.current.Use();

                int g = Undo.GetCurrentGroup();
                BTAbstractComposite parent = node.GetParent();
                Undo.RecordObject(node, "detach from parent");
                Undo.RecordObject(parent, "detach child");

                parent.DetachChild(node);

                EditorUtility.SetDirty(parent);
                EditorUtility.SetDirty(node);
                
                Undo.CollapseUndoOperations(g);

                editor.Repaint();

            }
            if (Event.current.type == EventType.KeyUp && Event.current.keyCode == KeyCode.E)
            {
                Event.current.Use();

                BTAbstractComposite parent = node.GetParent();
                BTTree tree = editor.ActiveTree;
     
                tree.RemoveNode(node);
                EditorUtility.SetDirty(tree);

                if (parent != null)
                {
                    parent.DetachChild(node);
                    EditorUtility.SetDirty(parent);
                }
                if (node is BTAbstractComposite)
                {
                    BTAbstractComposite n = node as BTAbstractComposite;

                    int childN = n.GetChildNum();
                    for (int i = 0; i < childN; ++i)
                    {
                        BTAbstractNode child = n.GetChild(i);
                        if (child != null)
                        {
                            n.DetachChild(i);
                            EditorUtility.SetDirty(child);
                        }
                        
                    }
                }
                GameObject.DestroyImmediate(node,true);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(tree));

                 
                editor.Repaint();

            }
           
        }
        public static void DrawDetailedDescription(UnityEngine.Object target, SerializedProperty detailedDescription)
        {
            EditorGUILayout.Space();
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("Detailed Description");
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            EditorGUILayout.BeginVertical();
            GUIStyle descStyle = new GUIStyle();
            descStyle.wordWrap = true; 
            descStyle.normal.textColor = Color.cyan;

            EditorGUI.BeginChangeCheck();
            string newDesc = EditorGUILayout.TextArea( detailedDescription.stringValue, descStyle);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "detailed desc");
                detailedDescription.stringValue = newDesc;
                EditorUtility.SetDirty(target);
            }
            EditorGUILayout.EndVertical();
            
        }
        public static void DrawParentFunction(BTAbstractComposite parent, BTAbstractNode node)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.ObjectField("Parent",parent, typeof(BTAbstractComposite),false);

            if (GUILayout.Button("Detach",GUILayout.MaxWidth(64)))
            {
                if (parent != null)
                {
                    int undoId = Undo.GetCurrentGroup();
                    Undo.RecordObject(node, "detach child");
                    Undo.RecordObject(parent, "detach child");

                    parent.DetachChild(node);
                    EditorUtility.SetDirty(node);
                    EditorUtility.SetDirty(parent);

                    Undo.CollapseUndoOperations(undoId);
                    EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        public static void DrawChildren(SerializedProperty childrenProp, BTAbstractComposite parent, List<BTAbstractNode> children, int min = int.MinValue, int max = int.MaxValue)
        {
            childrenProp.isExpanded = EditorGUILayout.Foldout(childrenProp.isExpanded, "Children");

            if (childrenProp.isExpanded)
            {
                EditorGUI.indentLevel++;
                for (int i = 0; i < children.Count; ++i)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.ObjectField(children[i], typeof(BTAbstractNode), false);

                    if (max > 1)
                    {
                        if (GUILayout.Button("↓", GUILayout.MaxWidth(24)))
                        {
                            MoveDownChildren(parent, children, i);
                        }
                        if (GUILayout.Button("↑", GUILayout.MaxWidth(24)))
                        {
                            MoveUpChildren(parent, children, i);
                        }
                    }

                    if (GUILayout.Button("D", GUILayout.MaxWidth(24)))
                    {
                        DetachChild(parent, children[i]);
                    }
                    if (GUILayout.Button("X", GUILayout.MaxWidth(24)))
                    {
                        RemoveChild(parent, children[i], i);
                    }
                    EditorGUILayout.EndHorizontal();

                }
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if (children.Count < max)
                {
                    if (GUILayout.Button("Push Front", GUILayout.MaxWidth(100)))
                    {
                        PushFront(parent);
                    }
                    if (GUILayout.Button("Push Back", GUILayout.MaxWidth(100)))
                    {
                        PushBack(parent);
                    }
                }
                EditorGUILayout.EndHorizontal();
                EditorGUI.indentLevel--;
            }
        }
        private static void DetachChild(BTAbstractComposite parent, BTAbstractNode child)
        {
            if (child != null)
            {
                int undoGroup = Undo.GetCurrentGroup();

                if (child != null)
                    Undo.RecordObject(child, "swap down");
                if (parent != null)
                    Undo.RecordObject(parent, "swap down");

                parent.DetachChild(child);

                if (child != null)
                    EditorUtility.SetDirty(child);
                if (parent != null)
                    EditorUtility.SetDirty(parent);

                Undo.CollapseUndoOperations(undoGroup);
                EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
            }
        }
        private static void PushFront(BTAbstractComposite parent)
        {
            Undo.RecordObject(parent, "swap down");
            parent.Children.Insert(0, null);

            EditorUtility.SetDirty(parent);
            EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
        }

        private static void PushBack(BTAbstractComposite parent)
        {
            Undo.RecordObject(parent, "swap down");
            parent.Children.Add(null);

            EditorUtility.SetDirty(parent);
            EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
        }
        private static void MoveUpChildren(BTAbstractComposite parent, List<BTAbstractNode> children, int i)
        {
            if (i > 0)
            {
                BTAbstractNode prevChild = children[i - 1];
                BTAbstractNode child = children[i];

                int undoGroup = Undo.GetCurrentGroup();

                if (child != null)
                    Undo.RecordObject(child, "swap up");
                if (prevChild != null)
                    Undo.RecordObject(prevChild, "swap up");
                if (parent != null)
                    Undo.RecordObject(parent, "swap up");

                if (prevChild != null)
                    parent.DetachChild(prevChild);
                if (child != null)
                    parent.DetachChild(child);

                parent.SetChild(child, i - 1);
                parent.SetChild(prevChild, i);

                if (child != null)
                    EditorUtility.SetDirty(child);
                if (prevChild != null)
                    EditorUtility.SetDirty(prevChild);
                if (parent != null)
                    EditorUtility.SetDirty(parent);

                Undo.CollapseUndoOperations(undoGroup);
                EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
            }
        }
        private static void RemoveChild(BTAbstractComposite parent, BTAbstractNode child, int index)
        {
            int undoGroup = Undo.GetCurrentGroup();

            if (child != null)
                Undo.RecordObject(child, "swap down");
            if (parent != null)
                Undo.RecordObject(parent, "swap down");

            if (child != null)
                parent.DetachChild(child);

            parent.RemoveChildAt(index);

            if (child != null)
                EditorUtility.SetDirty(child);
            if (parent != null)
                EditorUtility.SetDirty(parent);

            Undo.CollapseUndoOperations(undoGroup);
            EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
        }
        private static void MoveDownChildren(BTAbstractComposite parent, List<BTAbstractNode> children, int i)
        {
            if (i < children.Count - 1)
            {
                BTAbstractNode nextChild = children[i + 1];
                BTAbstractNode child = children[i];

                int undoGroup = Undo.GetCurrentGroup();

                if (child != null)
                    Undo.RecordObject(child, "swap down");
                if (nextChild != null)
                    Undo.RecordObject(nextChild, "swap down");
                if (parent != null)
                    Undo.RecordObject(parent, "swap down");

                if (nextChild != null)
                    parent.DetachChild(nextChild);
                if (child != null)
                    parent.DetachChild(child);

                parent.SetChild(child, i + 1);
                parent.SetChild(nextChild, i);

                if (child != null)
                    EditorUtility.SetDirty(child);
                if (nextChild != null)
                    EditorUtility.SetDirty(nextChild);
                if (parent != null)
                    EditorUtility.SetDirty(parent);

                Undo.CollapseUndoOperations(undoGroup);
                EditorWindow.FocusWindowIfItsOpen<BTEditorWindow>();
            }
        }
        public static void CleanSubTree(BTTree tree, BTAbstractNode root)
        {
            if (root == null)
                return;

            root.GetParent().RemoveChild(root);
            root.SetParent(null);

            tree.RemoveNode(root);

            if (root is BTAbstractComposite)
            {
                BTAbstractComposite comp = root as BTAbstractComposite;


                for (int i = 0; i < comp.Children.Count; ++i)
                {
                    if (comp.Children[i] != null)
                        CleanSubTree(tree, comp.Children[i]);
                }
                
            }
            GameObject.DestroyImmediate(root,true);
           
        }

        public static BTAbstractNode CloneSubTreeInto(BTAbstractNode rootNode, BTTree treeDst)
        {

            BTAbstractNode firstNode = CloneTree(rootNode, treeDst);

            BTRoot root = treeDst.Root;

            root.SetChild(firstNode,0);

            firstNode.SetParent(root);

            return firstNode;

        }
        public static BTTree CreateNewTree(string path)
        {
            BTTree tree = EditorUtils.CreateAssetAtPath<BTTree>(path);

            tree.Init();
            BTRoot root = ScriptableObject.CreateInstance<BTRoot>();
            root.name = "Root";
            root.Init();

            AssetDatabase.AddObjectToAsset(root, tree);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(root));

            tree.Root = root;
            tree.AddNode(root);

            EditorUtility.SetDirty(tree);
            AssetDatabase.SaveAssets();

            return tree;
        }
       
        public static BTAbstractNode CloneTreeContent(BTTree treeSrc, BTTree treeDst, bool includeRoot = false)
        {

            BTRoot root = treeSrc.Root;

            if (includeRoot)
                return CloneTree(root, treeDst);

            if (root.GetChildNum() == 0)
                return null;

            BTAbstractNode firstChild = root.GetChild(0);

            return CloneTree(firstChild, treeDst);
        }
        private static BTAbstractNode CloneTree(BTAbstractNode node, BTTree parentAsset)
        {
            if (node == null)
                return null;
            BTAbstractNode ret = null;
            if (node is BTAbstractComposite)
            {
                BTAbstractComposite parent = node as BTAbstractComposite;

                //clone current node serialized property but clear children
                BTAbstractComposite parentClone = ScriptableObject.Instantiate<BTAbstractComposite>(parent);
                parentClone.Children.Clear();


                foreach (BTAbstractNode child in parent.Children)
                {
                    BTAbstractNode childClone = CloneTree(child, parentAsset);

                    if (childClone != null)
                    {
                        childClone.SetParent(parentClone); 
                    }
                    parentClone.AddChild(childClone);
                    
                }
                ret = parentClone;

            }
            else
            {
                ret = ScriptableObject.Instantiate<BTAbstractNode>(node);
            }
            ret.name = node.name;
               
            parentAsset.AddNode(ret);

            

            
            ///
            AssetDatabase.AddObjectToAsset(ret, parentAsset);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(ret),ImportAssetOptions.ForceSynchronousImport);

           
            EditorUtility.SetDirty(parentAsset);

            AssetDatabase.Refresh();
           
            return ret;
        }

	}
}
