﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace BT.Unity.Editor
{
	public static class BTDrawer
	{
        public static void DrawNode(BTEditorWindow editor, BTAbstractNode node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            if (node is BTSequence)
            {
                DrawSequence(editor, node as BTSequence, handler, handle,drawStatus,s);
            }
            else if (node is BTRoot)
            {
                DrawRoot(editor, node as BTRoot, handler, handle, drawStatus, s);
            }
            else if (node is BTSelector)
            {
                DrawSelector(editor, node as BTSelector, handler, handle, drawStatus, s);
            }
            else if (node is BTRandomSelector)
            {
                DrawRandomSelector(editor, node as BTRandomSelector, handler, handle, drawStatus, s);
            }
            else if (node is BTParallel)
            {
                DrawParallel(editor, node as BTParallel, handler, handle, drawStatus, s);
            }
            else if (node is BTCondition)
            {
                DrawCondition(editor, node as BTCondition, handler, handle, drawStatus, s);
            }
            else if (node is BTMonitor)
            {
                DrawMonitor(editor, node as BTMonitor, handler, handle, drawStatus, s);
            }
            else if (node is BTAction)
            {
                DrawAction(editor, node as BTAction, handler, handle, drawStatus, s);
            }
            else if (node is BTActionSetter)
            {
                DrawSetter(editor, node as BTActionSetter, handler, handle, drawStatus, s);
            }
            else if (node is BTLog)
            {
                DrawLog(editor, node as BTLog, handler, handle);
            }
            else if (node is BTInverter)
            {
                DrawInverter(editor, node as BTInverter, handler, handle, drawStatus, s);
            }
            else if (node is BTRepeater)
            {
                DrawRepeater(editor, node as BTRepeater, handler, handle, drawStatus, s);
            }
            else if (node is BTReference)
            {
                DrawReference(editor, node as BTReference, handler, handle, drawStatus, s);
            }
        }
        private static void DrawReference(BTEditorWindow editor, BTReference node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTReference.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconReference, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Reference", GUILayout.MinWidth(node.GetWidth()));
            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);

            DrawInputSlots(editor, node, handler, b);

            
            DenormalizeBounds(editor, node, b);

        }
        private static void DrawRepeater(BTEditorWindow editor, BTRepeater node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTRepeater.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconRepeater, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Repeater", GUILayout.MinWidth(node.GetWidth()));
            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);

            DrawOutputSlots(editor, node, handler, b);
            DrawInputSlots(editor, node, handler, b);

            DrawOutputWires(editor, node, b);

            DenormalizeBounds(editor, node, b);

        }
        private static void DrawInverter(BTEditorWindow editor, BTInverter node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTInverter.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconInverter, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Inverter", GUILayout.MinWidth(node.GetWidth()));
            BTEditorUtils.DrawDescription(editor, node, b); 
           
            if(drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawOutputSlots(editor, node, handler, b);
            DrawInputSlots(editor, node, handler, b);

            DrawOutputWires(editor, node, b);

            DenormalizeBounds(editor, node, b);

        }
        private static void DrawLog(BTEditorWindow editor, BTLog node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTActionSetter.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    GUILayout.Label("", GUILayout.MinWidth(node.GetWidth()));
                    GUI.DragWindow();
                },
            "Log", GUILayout.MinWidth(node.GetWidth()));
            
            BTEditorUtils.DrawDescription(editor, node, b);
            
            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawInputSlots(editor, node, handler, b);

            DenormalizeBounds(editor, node, b);
        
        }

        private static void DrawSetter(BTEditorWindow editor, BTActionSetter node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTActionSetter.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconSetter, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Set", GUILayout.MinWidth(node.GetWidth()));
            
            BTEditorUtils.DrawDescription(editor, node, b);
            
            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawInputSlots(editor, node, handler, b);

            DenormalizeBounds(editor, node, b);
        }

        private static void DrawRoot(BTEditorWindow editor, BTRoot node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
           // node.SetWidth(BTSequence.m_minSize + node.GetChildNum() * BTRoot.m_childW);
       //     Debug.Log("before norm " + node.GetBounds());
            Rect b =  NormalizedBounds(editor, node);

         //   Debug.Log("norm " + b);
            
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    GUILayout.Label("", GUILayout.ExpandWidth(false), GUILayout.MinWidth(node.GetWidth()), GUILayout.MaxWidth(node.GetWidth()));
                    GUI.backgroundColor = Color.red;
                  //  GUILayout.Box("", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

                    GUI.DragWindow();
                },
            "Root", GUILayout.Width(node.GetWidth()));
            b.width = node.GetWidth();
           // Debug.Log("moved " + b);
            
            DrawOutputSlots(editor, node, handler,b);   
            DrawOutputWires(editor, node,b);

            node.SetBounds(b);
            DenormalizeBounds(editor, node,b);
           // Debug.Log("denorm " + b);
            
        }
        private static void DrawSequence(BTEditorWindow editor, BTSequence node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth( BTSequence.m_minSize + node.GetChildNum() * BTSequence.m_childW);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b, 
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor,node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconSequence, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                }, 
            "Sequence", GUILayout.MinWidth(node.GetWidth()));
           
            BTEditorUtils.DrawDescription(editor, node, b);
            
            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawOutputSlots(editor, node,handler,b);
            DrawInputSlots(editor, node, handler,b);

            DrawOutputWires(editor, node,b);

            DenormalizeBounds(editor, node,b);

        }
        private static void DrawSelector(BTEditorWindow editor, BTSelector node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTSelector.m_minSize + node.GetChildNum() * BTSelector.m_childW);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconSelector, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Selector", GUILayout.MinWidth(node.GetWidth()));

            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawOutputSlots(editor, node, handler, b);
            DrawInputSlots(editor, node, handler, b);

            DrawOutputWires(editor, node, b);

            DenormalizeBounds(editor, node, b);
        }
        private static void DrawRandomSelector(BTEditorWindow editor, BTRandomSelector node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTSelector.m_minSize + node.GetChildNum() * BTSelector.m_childW);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconSelector, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal();

                    GUI.DragWindow();
                },
            "RandSelector", GUILayout.MinWidth(node.GetWidth()));

            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);

            DrawOutputSlots(editor, node, handler, b);
            DrawInputSlots(editor, node, handler, b);

            DrawOutputWires(editor, node, b);

            DenormalizeBounds(editor, node, b);
        }
        private static void DrawParallel(BTEditorWindow editor, BTParallel node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTParallel.m_minSize + node.GetChildNum() * BTParallel.m_childW);
//            node.SetHeight(BTParallel.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
      
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconParallel, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64),GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    GUI.DragWindow();
                },
            "Parallel", GUILayout.MinWidth(node.GetWidth()));
            
            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawOutputSlots(editor, node, handler, b);
            DrawInputSlots(editor, node, handler, b);

            DrawOutputWires(editor, node, b);

            DenormalizeBounds(editor, node, b);
        }
        private static void DrawCondition(BTEditorWindow editor, BTCondition node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTCondition.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconCondition, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Condition", GUILayout.MinWidth(node.GetWidth()));
            
            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawInputSlots(editor, node, handler, b);

            DenormalizeBounds(editor, node, b);
        }
        private static void DrawMonitor(BTEditorWindow editor, BTMonitor node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTMonitor.m_minSize);

            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconMonitor, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Monitor", GUILayout.MinWidth(node.GetWidth()));
            
            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawInputSlots(editor, node, handler, b);

            DenormalizeBounds(editor, node, b);
        }
        private static void DrawAction(BTEditorWindow editor, BTAction node, BTEventHandler handler, int handle, bool drawStatus = false, Status s = Status.NONE)
        {
            node.SetWidth(BTAction.m_minSize);
//            node.SetHeight(BTAction.m_minSize);
            Rect b = NormalizedBounds(editor, node);
            b = GUILayout.Window(handle, b,
                (int h) =>
                {
                    BTEditorUtils.BTNodeHandleSelection(editor, node);
                    BTEditorUtils.BTNodeHandleInput(editor, node);
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    GUILayout.Box(editor.Resources.IconAction, GUILayout.MaxHeight(64), GUILayout.MaxWidth(64), GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false));// GUILayout.ExpandWidth(true));
                    GUILayout.FlexibleSpace();
                    EditorGUILayout.EndHorizontal(); 
                    
                    GUI.DragWindow();
                },
            "Action", GUILayout.MinWidth(node.GetWidth()));
            
            BTEditorUtils.DrawDescription(editor, node, b);

            if (drawStatus)
                BTEditorUtils.DrawStatus(editor, s, b);
            
            DrawInputSlots(editor, node, handler, b);

            DenormalizeBounds(editor, node, b);
        }
        private static void DrawOutputSlots(BTEditorWindow editor, BTAbstractComposite node, BTEventHandler handler, Rect bounds)
        {
            int index = BTEditorUtils.DrawSlotHandles(bounds, BTEditorUtils.Side.DOWN, node.GetChildNum());

            if (index >= 0)
            {
                if (node.GetChildNum() > index && node.GetChild(index) == null)
                    handler(node, BTEditorEvent.ATTACH_CHILD_BEGIN, index);
                else
                    handler(node, BTEditorEvent.CHILD_OP_ERR);
            }

        }
        private static void DrawInputSlots(BTEditorWindow editor, BTAbstractNode node, BTEventHandler handler, Rect bounds)
        {
            int index = BTEditorUtils.DrawSlotHandles(bounds, BTEditorUtils.Side.TOP, 1);
            if (index >= 0)
            {
                if (!node.HasParent())
                    handler(node, BTEditorEvent.ATTACH_CHILD_END, index);
                else
                    handler(node, BTEditorEvent.CHILD_OP_ERR);
            }

        }
        private static void DrawOutputWires(BTEditorWindow editor, BTAbstractComposite node, Rect bounds)
        {
            int childNum = node.GetChildNum();
            for (int i = 0; i < childNum; ++i)
            {
                BTAbstractNode c = node.GetChild(i);
                if (c == null)
                    continue;
                Rect childBounds = c.GetBounds();
                childBounds.center += new Vector2(BTEditorWindow.TREE_PANEL_OFFSET_W, 0f);
                BTEditorUtils.DrawSlotWire(bounds, BTEditorUtils.Side.DOWN, i, childNum,
                                    childBounds, BTEditorUtils.Side.TOP, 1, 1);
            }
        }

        private static Rect NormalizedBounds(BTEditorWindow window, BTAbstractNode node)
        {
            Rect b = node.GetBounds();
            b.x += BTEditorWindow.TREE_PANEL_OFFSET_W;
            return b;
        }
        private static void DenormalizeBounds(BTEditorWindow window, BTAbstractNode node, Rect normBounds)
        {
            Rect newB = normBounds;
            newB.x -= BTEditorWindow.TREE_PANEL_OFFSET_W;

            node.SetBounds(newB);
        
        }
	}
}
