﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using UnityEditor;

namespace BT.Unity.Editor
{
   
    [CustomPropertyDrawer(typeof(ConditionAttribute))]
	public class BTConditionHolderDrawer : PropertyDrawer
	{
        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
            SerializedProperty valueName = property.FindPropertyRelative("valueName");
            SerializedProperty valueType = property.FindPropertyRelative("valueType");
            SerializedProperty boolVal = property.FindPropertyRelative("boolVal");
            SerializedProperty intVal = property.FindPropertyRelative("intVal");
            SerializedProperty floatVal = property.FindPropertyRelative("floatVal");
            SerializedProperty comparerType = property.FindPropertyRelative("comparer");
            SerializedProperty watch = property.FindPropertyRelative("watch");

            EditorGUILayout.PropertyField(valueName);
        
            BT.BBValueType type = (BT.BBValueType) valueType.intValue;
         
            EditorGUI.BeginChangeCheck();
            BT.BBValueType newT = (BT.BBValueType)EditorGUILayout.EnumPopup(valueType.name, type);
            if(EditorGUI.EndChangeCheck())
            {
                valueType.intValue = (int)newT;
            }
      
            switch (type)
            {
                case BBValueType.BOOL:
                {
                    EditorGUILayout.PropertyField(boolVal);
                    break;
                }
                case BBValueType.FLOAT:
                {
                    EditorGUILayout.PropertyField(floatVal);
                    break;
                }
                case BBValueType.INT:
                {
                    EditorGUILayout.PropertyField(intVal);
                    break;
                }

            }
            BT.Comparer comparer = (BT.Comparer)comparerType.intValue;
            EditorGUI.BeginChangeCheck();
            BT.Comparer newComp = (BT.Comparer)EditorGUILayout.EnumPopup(comparerType.name, comparer);
            if (EditorGUI.EndChangeCheck())
            {
                comparerType.intValue = (int)newComp;
            }
            BBLocation w = (BBLocation)watch.intValue;
            EditorGUI.BeginChangeCheck();
            BBLocation newWatch = (BBLocation)EditorGUILayout.EnumPopup(watch.name, w);
            if (EditorGUI.EndChangeCheck())
            {
                watch.intValue = (int)newWatch;
            }
            
         
        }
	}
}
