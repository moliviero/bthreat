﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using UnityEditor;
using BT.Unity;
using System.Collections.Generic;
using EditorExtension;

namespace BT.Unity.Editor
{
	public class BTCompositeInspector<T> : BTNodeInspector<T>
        where T : BTAbstractComposite
	{
        protected SerializedProperty child_prop;

        protected override void OnEnable()
        {
            base.OnEnable();
            child_prop = SerializedObject["m_children"];
        }
        public override void OnInspectorGUI()
        {
            serializedObject.UpdateIfDirtyOrScript();
            BTEditorUtils.DrawChildren(child_prop, Target, Target.Children, 1, Target.MaxChildCount);
            base.OnInspectorGUI();
          
            serializedObject.ApplyModifiedProperties();

        }
        
	}
}
