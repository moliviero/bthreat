﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BT.Unity.Editor
{
    [CustomEditor(typeof(BTSequence))]
	public class BTSequenceEditor : BTCompositeInspector<BTSequence>
	{

        public const string TOOL_TIP =
            "A sequence node execute all children in order from left to right. A children must complete" +
            "with success in order to schedule next child execution. A sequence returns SUCCESS if all " +
            "children return SUCCESS, otherwise returns FAILURE when the first children fail.";
        protected override string HowTo { get { return TOOL_TIP; } }        
	}
}
