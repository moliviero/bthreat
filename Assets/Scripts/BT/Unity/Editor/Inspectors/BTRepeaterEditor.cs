﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BT.Unity.Editor
{
    [CustomEditor(typeof(BTRepeater))]
    public class BTRepeaterEditor : BTCompositeInspector<BTRepeater>
    {
        private const string TOOL_TIP =
             "Repeater node allow you to repeat the child node until condition is valid." +
             "It offers several possible customization. CONST: repeat child n times. It returns COMPLETED when" + 
             "all iterations have be exectued successfully. CONDITION: execute child once. " +
             "It repeat the child execution until it fails. Every repetition the condition is evalutated." +
             "When condition is no more satisfied the repeater return with the last execution status of the child (probably success). " +
             "MONITOR: repeat child execution until monitored condition is true or child fails. It return always FAILED";
        private SerializedProperty type_prop;
        private SerializedProperty condition_prop;
        private SerializedProperty repeatNum_prop;

        protected override string HowTo { get { return TOOL_TIP; } }

        private static readonly string[] EXCLUDE_PROP = { "m_conditionHolder", "m_repeatNum", "m_type" };
        protected override string[] ExcludedRenderProperties 
        { 
            get 
            {
                return EXCLUDE_PROP;
            } 
        }
        private new void OnEnable()
        {
            base.OnEnable();

            type_prop = SerializedObject["m_type"];
            condition_prop = SerializedObject["m_conditionHolder"];
            repeatNum_prop = SerializedObject["m_repeatNum"];

        }
        public override void OnInspectorGUI()
        {

            serializedObject.UpdateIfDirtyOrScript();
            EditorGUILayout.PropertyField(type_prop);

            if (Target.ConditionType == Repeater.Type.CONST)
            {
                EditorGUILayout.PropertyField(repeatNum_prop);
            }
            else
            {
                EditorGUILayout.PropertyField(condition_prop);
            }

            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();
        }
    }
}