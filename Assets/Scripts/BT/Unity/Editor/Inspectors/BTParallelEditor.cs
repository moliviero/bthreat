﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BT.Unity.Editor
{
    [CustomEditor(typeof(BTParallel))]
    public class BTParallelEditor : BTCompositeInspector<BTParallel>
    {
        private const string TOOL_TIP =
           "A PARALLEL node execute all children from left to right at every update." +
           "You can specify the number of children that must fail and succeed in order to trigger"+
           "the PARALLEL node success or failure. Typically a PARALLEL node fails when a single child fails and succeed " + 
           "when all child succeed";

        protected override string HowTo { get { return TOOL_TIP; } }

    }
}
