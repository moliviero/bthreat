﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using System.Collections;
using EditorExtension;
using UnityEditor;
using System.Linq;

namespace BT.Unity.Editor
{
    public abstract class BTNodeInspector<T> : EditorExtended<T>
        where T : BTAbstractNode
    {

        protected GUIStyle descStyle;
        protected SerializedProperty detailedDesc_prop;
        protected SerializedProperty parent_prop;
        protected SerializedProperty desc_prop;

        protected virtual string[] ExcludedRenderProperties { get { return Enumerable.Empty<string>().ToArray(); } }
        protected string[] exclude_prop = {"m_Script"};
        protected virtual string HowTo { get { return ""; } }        
        
        protected override void OnEnable()
        {
            descStyle = new GUIStyle();
            descStyle.normal.textColor = Color.magenta;
            descStyle.wordWrap = true;

            detailedDesc_prop = SerializedObject["m_detailedDescription"];
            parent_prop = SerializedObject["m_parent"];
            desc_prop = SerializedObject["description"];

            
        }
        public override void OnInspectorGUI()
        {
           // base.OnInspectorGUI();
            serializedObject.UpdateIfDirtyOrScript();
            DrawPropertiesExcluding(serializedObject, exclude_prop.Concat(ExcludedRenderProperties).ToArray());
         
            BTEditorUtils.DrawParentFunction(parent_prop.objectReferenceValue as BTAbstractComposite, Target);

            EditorGUILayout.PropertyField(desc_prop);
            BTEditorUtils.DrawDetailedDescription(Target, detailedDesc_prop);
            BTEditorUtils.DrawHowToInspector(HowTo, descStyle);

            serializedObject.ApplyModifiedProperties();
        }
    }
}