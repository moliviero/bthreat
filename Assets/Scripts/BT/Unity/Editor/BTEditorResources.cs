﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using UnityEditor;

namespace BT.Unity.Editor
{
	public class BTEditorResources
	{
        public Texture2D IconSelector { get; private set; }
        public Texture2D IconRandomSelector { get; private set; }
        public Texture2D IconSequence { get; private set; }
        public Texture2D IconParallel { get; private set; }
        public Texture2D IconCondition { get; private set; }
        public Texture2D IconMonitor { get; private set; }
        public Texture2D IconAction { get; private set; }
        public Texture2D IconSetter { get; private set; }
        public Texture2D IconInverter { get; private set; }
        public Texture2D IconRepeater { get; private set; }
        public Texture2D IconReference { get; private set; }


        public BTEditorResources()
        {
            IconSelector = EditorGUIUtility.Load("BT/bt_selector.png") as Texture2D;
            IconRandomSelector = EditorGUIUtility.Load("BT/bt_rand_selector.png") as Texture2D;
            IconSequence = EditorGUIUtility.Load("BT/bt_sequence.png") as Texture2D;
            IconParallel = EditorGUIUtility.Load("BT/bt_parallel.png") as Texture2D;
            IconCondition = EditorGUIUtility.Load("BT/bt_condition.png") as Texture2D;
            IconMonitor = EditorGUIUtility.Load("BT/bt_monitor.png") as Texture2D;
            IconAction = EditorGUIUtility.Load("BT/bt_action.png") as Texture2D;
            IconSetter = EditorGUIUtility.Load("BT/bt_setter.png") as Texture2D;
            IconInverter = EditorGUIUtility.Load("BT/bt_inverter.png") as Texture2D;
            IconRepeater = EditorGUIUtility.Load("BT/bt_repeater.png") as Texture2D;
            IconReference = EditorGUIUtility.Load("BT/bt_reference.png") as Texture2D;
           
        }
	}
}
