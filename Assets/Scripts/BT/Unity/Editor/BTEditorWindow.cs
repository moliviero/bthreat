﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.IO;
using EditorExtension;
using UnityEditor.Callbacks;

namespace BT.Unity.Editor
{
	public class BTEditorWindow : EditorWindow
	{
        private const string LAST_OPENED_BT_KEY = "LAST_OPENED_BT";
        private const float LEFT_PANEL_W = 300;
        private const float DIVISOR_W = 8;
        public const float TREE_PANEL_OFFSET_W = LEFT_PANEL_W + DIVISOR_W;
        private const float TREE_PANEL_MAX_W = 3000;
        private const float TREE_PANEL_MAX_H = 3000;


        private float m_zoom = 1f;

        public enum Status { EDIT,ATTACCH_CHILD, DEBUG}
        
        private Vector2 m_scrollPos;
        private Vector2 m_nodePanelScrollPos;

        private BTEditorResources m_resources;
        public BTEditorResources Resources
        {
            get
            {
                if (m_resources == null)
                    m_resources = new BTEditorResources();
                return m_resources;
            }
        }
      //  private IBTComponent seq;
        private BTTree m_activeTree;
        public BTTree ActiveTree
        {
            get
            {
                return m_activeTree;
            }
        }
       
        private Status m_status;

        private BTAbstractNode m_nodeSrc;
        private int m_childIndex;
        private Vector2 m_startMousePos;
        private string m_selectedTree = "";
        private int m_selectedTreeIndex = -1;

        private bool m_wasPlaying = false;

        private GameObject m_debugAgentTarget;
        private bool m_debugActive;
        private IBTAgent m_agent;
        private ITree m_debugTree;
        private Dictionary<int, BT.Status> m_tasksStatusMap;

        
        private void HandleInput()
        {
            if (Event.current.type == EventType.MouseUp)
            {
//                Debug.Log("id: " + GUIUtility.hotControl);
                m_status = Status.EDIT;
                m_nodeSrc = null;
                wantsMouseMove = false;
                Repaint();
            }
            if (Event.current.type == EventType.KeyUp && Event.current.control && Event.current.keyCode == KeyCode.Z)
            {
                Repaint();
            }
        
        }
        private void HandleBTEvent()
        {
            if (m_status == Status.ATTACCH_CHILD)
            {
                if (m_nodeSrc != null)
                {
                    Vector3 startPos = new Vector3(m_startMousePos.x - m_scrollPos.x, m_startMousePos.y - m_scrollPos.y, 0f);
                    Vector3 startTan = startPos + Vector3.up * 50f;
                    Vector3 endPos = new Vector3(Event.current.mousePosition.x, Event.current.mousePosition.y, 0f);
                    Vector3 endTan = endPos - Vector3.up * 50f;

                    Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.red, null, 3f);

                    if (Event.current.type == EventType.MouseMove)
                        Repaint();
                }
            }
        }
        void OnGUI()
        {
            LeftPanel();
            TreePanel();

          
            HandleBTEvent();
            HandleInput();
         
        }
        void DoWindow(int foo) 
        {
            
            GUILayout.Button("Hi");
            GUI.DragWindow();
        }

        void OnEnable()
        {
            if (EditorPrefs.HasKey(LAST_OPENED_BT_KEY) && m_activeTree == null)
            {
                LoadTree(EditorPrefs.GetString(LAST_OPENED_BT_KEY));
            }
        }

        private void LoadTree(string tree)
        {
           
            string[] trees = BehaviorTrees;
            m_selectedTreeIndex = Array.FindIndex(trees, (string name) => name == tree);
            if (m_selectedTreeIndex < 0)
                return;
            m_selectedTree = trees[m_selectedTreeIndex];
            m_activeTree = AssetDatabase.LoadAssetAtPath(m_selectedTree, typeof(BTTree)) as BTTree;
            EditorPrefs.SetString(LAST_OPENED_BT_KEY, m_selectedTree);
        }
        private void TreeDBPanel()
        {
            EditorGUI.BeginChangeCheck();
            string[] trees = BehaviorTrees;
            m_selectedTreeIndex = EditorGUILayout.Popup("Open Tree", m_selectedTreeIndex, trees, GUILayout.MinWidth(256f));
            if (EditorGUI.EndChangeCheck())
            {
                m_selectedTree = trees[m_selectedTreeIndex];
                m_activeTree = AssetDatabase.LoadAssetAtPath(m_selectedTree, typeof(BTTree)) as BTTree;
                EditorPrefs.SetString(LAST_OPENED_BT_KEY, m_selectedTree);
            }

            if (m_activeTree != null && GUILayout.Button("Save Tree As..", GUILayout.MaxWidth(128)))
            {
                SaveAsPanel();
            }
            if(m_activeTree != null && Selection.activeObject is BTAbstractComposite )
            {
                if (GUILayout.Button("Save SubTree As..", GUILayout.MaxWidth(128)))
                {
                    SaveSubTreeAsPanel(Selection.activeObject as BTAbstractNode);
                }
                
            }
            if (m_activeTree != null && Selection.activeObject is BTReference)
            {
                if (GUILayout.Button("Merge Subtree", GUILayout.MaxWidth(128)))
                {
                    Debug.Log("Not implemented");
                }

            }
            EditorGUILayout.BeginHorizontal();
            GUI.color = Color.white;
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.MaxHeight(2f));
            EditorGUILayout.EndHorizontal();
        }
        private void SaveSubTreeAsPanel(BTAbstractNode selectedNode)
        {
            string path = EditorUtility.SaveFilePanelInProject("Save BTTree as", "NewBehaviorTree", "asset", "Enter the new BT name");
            if (path.Length != 0)
            {
                BTTree assetAtTargetPath = AssetDatabase.LoadAssetAtPath(path, typeof(BTTree)) as BTTree;

                //create a new tree
                if (assetAtTargetPath == null)
                {
                    assetAtTargetPath = BTEditorUtils.CreateNewTree(path);
                }
                else
                {
                    //need to clean target tree and copy over the selected subtree
                    assetAtTargetPath.Clear();
                }
                BTEditorUtils.CloneSubTreeInto(selectedNode, assetAtTargetPath);

                BTAbstractComposite parent = selectedNode.GetParent();
                Vector2 pos = selectedNode.GetPosition();
                BTEditorUtils.CleanSubTree(m_activeTree, selectedNode);
                //create reference
                BTReference refNode = ScriptableObject.CreateInstance<BTReference>();

                refNode.name = "Reference";
                refNode.Init();
                refNode.SetPosition(pos);
                m_activeTree.AddNode(refNode);
                AssetDatabase.AddObjectToAsset(refNode, m_activeTree);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(refNode), ImportAssetOptions.ForceSynchronousImport);

                if (parent != null)
                {
                    parent.AddChild(refNode);
                    refNode.SetParent(parent);
                }
                refNode.TreeReference = assetAtTargetPath;
                EditorUtility.SetDirty(m_activeTree);
                
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceSynchronousImport);

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                
            }

        }
    
        private void SaveAsPanel()
        {
            string path = EditorUtility.SaveFilePanelInProject("Save BTTree as","NewBehaviorTree", "asset", "Enter the new BT name");
            if (path.Length != 0)
            {
                BTTree assetAtTargetPath = AssetDatabase.LoadAssetAtPath(path, typeof(BTTree)) as BTTree;

                if (assetAtTargetPath == null)
                    AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(m_activeTree.GetInstanceID()), path);
                else
                {
                    assetAtTargetPath.Clear();
                    assetAtTargetPath.Root = BTEditorUtils.CloneTreeContent(m_activeTree,assetAtTargetPath, true) as BTRoot;
                 //   Debug.LogError("Copying over an existing tree is not currently supported");
                }
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceSynchronousImport);

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
                m_activeTree = AssetDatabase.LoadAssetAtPath(path, typeof(BTTree)) as BTTree;

                string[] treeList = BehaviorTrees;

                int index = Array.FindIndex(treeList, tName => tName == path);
                m_selectedTreeIndex = index;
            }

        }
        private void TreeNodesPanel()
        {
            BTAbstractNode newNode = null;

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.LabelField("Create Node");
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Sequence", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTSequence s = ScriptableObject.CreateInstance<BTSequence>();
                s.name = "Sequence";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Selector", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTSelector s = ScriptableObject.CreateInstance<BTSelector>();
                s.name = "Selector";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Random Selector", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTRandomSelector s = ScriptableObject.CreateInstance<BTRandomSelector>();
                s.name = "RandomSelector";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Parallel", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTParallel s = ScriptableObject.CreateInstance<BTParallel>();
                s.name = "Parallel";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Condition", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTCondition s = ScriptableObject.CreateInstance<BTCondition>();
                s.name = "Condition";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Monitor", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTMonitor s = ScriptableObject.CreateInstance<BTMonitor>();
                s.name = "Monitor";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Action", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTAction s = ScriptableObject.CreateInstance<BTAction>();
                s.name = "Action";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Setter", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTActionSetter s = ScriptableObject.CreateInstance<BTActionSetter>();
                s.name = "ActionSetter";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Inverter", GUILayout.MaxWidth(128f)))
            {
                BTInverter s = ScriptableObject.CreateInstance<BTInverter>();
                s.name = "Inverter";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Repeater", GUILayout.MaxWidth(128f)))
            {
                BTRepeater s = ScriptableObject.CreateInstance<BTRepeater>();
                s.name = "Repeater";
                s.Init();
                newNode = s;
            }
            if (GUILayout.Button("Reference", GUILayout.MaxWidth(128f)))
            { 
                BTReference r = ScriptableObject.CreateInstance<BTReference>();
                r.name = "Reference";
                r.Init();
                newNode = r;
            }
            EditorGUILayout.BeginHorizontal();
            GUI.color = Color.white;
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.MaxHeight(2f));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.Space();

            if (GUILayout.Button("Create Log", GUILayout.MaxWidth(128f)))
            {
                //TODO: use factory
                BTLog s = ScriptableObject.CreateInstance<BTLog>();
                s.name = "Log";
                s.Init();
                newNode = s;
            }
            if (newNode != null)
            {
                int group = Undo.GetCurrentGroup();
                Undo.RegisterCreatedObjectUndo(newNode, "create node");
                Undo.RecordObject(m_activeTree, "add new node");

                newNode.SetPosition(new Vector2(100f, 100f) + m_scrollPos);
                m_activeTree.AddNode(newNode);

                AssetDatabase.AddObjectToAsset(newNode, m_activeTree);
                AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(newNode));

                
                EditorUtility.SetDirty(m_activeTree);
                Undo.CollapseUndoOperations(group);
            }

        }
        private void LeftPanel()
        {
            
            EditorGUILayout.BeginHorizontal(GUILayout.MinWidth(LEFT_PANEL_W));
            m_nodePanelScrollPos = GUILayout.BeginScrollView(m_nodePanelScrollPos,false,true);

            EditorGUILayout.BeginVertical();

        //    m_actionTex = EditorGUILayout.ObjectField("tex",m_actionTex,typeof(Texture2D)) as Texture2D;
            TreeDBPanel();
            TreeNodesPanel();

            m_zoom = GUILayout.HorizontalSlider(m_zoom,0.25f,1f);
            DebugPanel();

            GUILayout.FlexibleSpace();

            GUILayout.EndScrollView();

            EditorGUILayout.EndVertical();

            //GUILayout.FlexibleSpace();

            //GUI.color = Color.white;
            //GUILayout.Box("", GUILayout.ExpandHeight(true));
            EditorGUILayout.EndHorizontal();

           
        }
        private void DebugPanel()
        {
            if(true)// (m_status == Status.DEBUG)
            { 
                EditorGUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                EditorGUILayout.LabelField("DEBUG", GUILayout.MaxWidth(128));
                GUILayout.FlexibleSpace();
                EditorGUILayout.EndHorizontal();

                if(!m_debugActive) 
                {
                    EditorGUI.BeginChangeCheck();
                    
                    m_debugAgentTarget = EditorGUILayout.ObjectField("BTAgent",m_debugAgentTarget, typeof(GameObject), true) as GameObject;

                    if (EditorGUI.EndChangeCheck() && m_debugAgentTarget != null)
                    {
                        m_agent = UnityUtils.GetComponentImplementingInterface<IBTAgent>(m_debugAgentTarget);

                        if (m_agent == null)
                        {
                            m_debugAgentTarget = null;
                            return;
                        }
                        

                    }
                    if (m_debugAgentTarget != null && m_agent == null)
                    { 
                        //lost unserialized ref
                        m_agent = UnityUtils.GetComponentImplementingInterface<IBTAgent>(m_debugAgentTarget);

                    }

                }
                if (!m_debugActive && m_agent != null && GUILayout.Button("Start Debug"))
                {
                    m_debugActive = true;
                    InitDebugger();
                }
                if (m_debugActive && GUILayout.Button("Stop Debug"))
                {
                    m_debugActive = false;
                }
            }
        }

        private void InitDebugger()
        {
            m_debugTree = m_agent.GetTree();
            m_tasksStatusMap = new Dictionary<int, BT.Status>();

            foreach (BTAbstractNode n in m_activeTree.Nodes)
            {
                m_tasksStatusMap[n.TrackedNodeId] = BT.Status.NONE;
            }
            IScheduler scheduler = m_debugTree.GetScheduler();

            foreach (ITask t in scheduler.GetRunningTasks())
            {
                m_tasksStatusMap[t.SkeletonNode().GetId()] = t.GetStatus();
            }
            foreach (ITask t in scheduler.GetTerminatedTasks())
            {
                m_tasksStatusMap[t.SkeletonNode().GetId()] = t.GetStatus();
            }
            m_agent.GetTree().GetScheduler().OnUpdateTask += OnTaskUpdated;
            m_agent.GetTree().GetScheduler().OnRemove += OnTaskUpdated;
        }
        private void TreePanel()
        {

            GUILayout.BeginArea(new Rect(TREE_PANEL_OFFSET_W, 0, position.width - TREE_PANEL_OFFSET_W, position.height));

            GUI.Label(new Rect(2f, 2f, 500f, 500f), m_selectedTree);
            m_scrollPos = GUI.BeginScrollView(new Rect(0, 0, position.width - TREE_PANEL_OFFSET_W, position.height), m_scrollPos, new Rect(TREE_PANEL_OFFSET_W, 0, TREE_PANEL_MAX_W, TREE_PANEL_MAX_H));

           // GUIUtility.ScaleAroundPivot(Vector2.one * m_zoom, new Vector2((position.width - TREE_PANEL_OFFSET_W) * 0.5f, position.height));
      
            BeginWindows();

      
            if (m_activeTree != null)
            {
               
                foreach (BTAbstractNode b in m_activeTree.Nodes)
                {
                    if (m_debugActive)
                    {
                        BTDrawer.DrawNode(this, b, HandleEvent, b.GetInstanceID(), m_debugActive, m_tasksStatusMap[b.TrackedNodeId]);
                    }
                    else
                    {
                        BTDrawer.DrawNode(this, b, HandleEvent, b.GetInstanceID());
                    }
                }
            }
            EndWindows(); // Close the scroll view 
            GUI.EndScrollView();
            GUILayout.EndArea();
       
        }

        private void HandleEvent(BTAbstractNode sourceNode, BTEditorEvent e, params object[] args)
        {
            switch (e)
            {
                case BTEditorEvent.ATTACH_CHILD_BEGIN:
                {
                    if (m_status != Status.EDIT)
                        return;
                    m_status = Status.ATTACCH_CHILD;
                    m_nodeSrc = sourceNode;
                    m_childIndex = Convert.ToInt32(args[0]);
                    m_startMousePos = Event.current.mousePosition;
                    wantsMouseMove = true;
                    break;
                }
                case BTEditorEvent.ATTACH_CHILD_END:
                {
                    if (m_status != Status.ATTACCH_CHILD)
                        return;

                    BTAbstractComposite parent = m_nodeSrc as BTAbstractComposite;

                    Undo.RecordObject(parent, "add child");
                    parent.SetChild(sourceNode, m_childIndex);
                    EditorUtility.SetDirty(parent);
                    Undo.RecordObject(sourceNode, "set parent");
                    sourceNode.SetParent(parent);
                    EditorUtility.SetDirty(sourceNode);
                    m_nodeSrc = null;
                    m_childIndex = -1;
                    m_status = Status.EDIT;
                    break; 
                }
                case BTEditorEvent.CHILD_OP_ERR:
                {
                    m_nodeSrc = null;
                    m_childIndex = -1;
                    m_status = Status.EDIT;
                    break;
                }
            }
        }

        private string[] BehaviorTrees
        {
            get
            {
                string[] guids = AssetDatabase.FindAssets("t:BTTree", null);
                string[] bts = guids.Select(guid => AssetDatabase.GUIDToAssetPath(guid)).ToArray();

                return bts;
            }
        }
        [MenuItem("Tools/Behavior Tree Editor")]
        private static void Init()
        {
            EditorWindow.GetWindow<BTEditorWindow>();

        }

        void Update()
        {
         //   Debug.Log("up");
            if (EditorApplication.isPlaying)
            {
                if (!m_wasPlaying)
                {
                    m_status = Status.DEBUG;
                    Debug.Log("DEBUG ENABLED");
                }
                m_wasPlaying = true;
             }
            else if (!EditorApplication.isPlaying)
            {
                if (m_wasPlaying)
                {
                    m_status = Status.EDIT;
                    m_debugActive = false;
                    Debug.Log("DEBUG DISABLED");
                    Repaint();
                }
                m_wasPlaying = false;
            }
        }
        private void OnTaskUpdated(ITask t)
        {
            m_tasksStatusMap[t.SkeletonNode().GetId()] = t.GetStatus();
            this.Repaint();
        }
        
        [MenuItem("Assets/Create/Behavior Tree")]
        public static void CreateAsset()
        {
            BTTree tree = EditorUtils.CreateAssetAtCurrentPath<BTTree>("BehaviorTree");

            tree.Init();
            BTRoot root = ScriptableObject.CreateInstance<BTRoot>();
            tree.hideFlags = HideFlags.HideInHierarchy;

            BTRepeater  rep = ScriptableObject.CreateInstance<BTRepeater>();
            rep.Init();
            rep.name = "Repeater";
            root.name = "Root";
            root.Init();

            bool ismain = AssetDatabase.IsMainAsset(tree.GetInstanceID());
            root.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(root, tree);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(root));
            ismain = AssetDatabase.IsMainAsset(tree.GetInstanceID());
            rep.hideFlags = HideFlags.HideInHierarchy;
            AssetDatabase.AddObjectToAsset(rep, tree);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(rep));

            tree.Root= root;
            tree.AddNode(root);
            tree.AddNode(rep);

            EditorUtility.SetDirty(tree);
            AssetDatabase.SaveAssets();
            
      
            bool irMainRoot = AssetDatabase.IsMainAsset(rep.GetInstanceID());
            var treePath = AssetDatabase.GetAssetPath(tree);
            var nodePath = AssetDatabase.GetAssetPath(rep);
            var foo = AssetDatabase.LoadMainAssetAtPath(treePath);
            var bar = AssetDatabase.LoadMainAssetAtPath(nodePath);
          
        }
        [OnOpenAssetAttribute(1)]
        public static bool step1(int instanceID, int line)
        {
            var obj = EditorUtility.InstanceIDToObject(instanceID);
            Debug.Log("try to open " + obj.GetType());
          //  UnityEditor.ProjectWindowCallback.
            return false;
        }

    }
}
