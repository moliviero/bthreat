﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;

namespace BT.Unity
{
	public class BTLog : BTAbstractNode
	{
        public const float m_minSize = 50f;
        public const float m_childW = 32f;

        [SerializeField]
        private string m_msg;
        public string Message { get { return m_msg; } }


        public void Init()
        {
            m_pos = new Rect(0, 0, m_minSize, m_minSize);
        }
	}
}
