﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

namespace BT
{
    public class RandomSelector : AbstractComposite, ISelector
    {
        protected float[] m_childProbability;
        public float[] Probabilities
        {
            get
            {
                return m_childProbability;
            }
        }
        //protected float m_probabilityTotal;
        //public float ProbabilityTotal
        //{
        //    get
        //    {
        //        return m_probabilityTotal;
        //    }
        //}
        public RandomSelector(params float[] probs)
        { 
            m_childProbability = new float[probs.Length];
            System.Array.Copy(probs, m_childProbability, probs.Length);

            //m_probabilityTotal = 0f;
            //for (int i = 0; i < probs.Length; ++i)
            //{
            //    m_probabilityTotal += probs[i];
            //}
            //m_childProbability[0] = probs[0] / m_probabilityTotal;
            //for (int i = 1; i < probs.Length - 1; ++i)
            //{
            //    m_childProbability[i] = m_childProbability[i - 1] + probs[i] / m_probabilityTotal;
            //}
            //m_childProbability[m_childProbability.Length - 1] = 1f;

        }
        public override ITask CreateTask(ITree bt)
        {
            return new RandomSelectorTask(bt, this);
        }
    }
}
 