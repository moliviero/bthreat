﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
    public class Repeater : AbstractComposite, IRepeater
	{
        public enum Type { CONDITION, MONITOR, CONST}
        private BBValueType m_compType;
        private Comparer m_comparer;
        private BBLocation m_bbType;
  
        private string m_valueName;
        private object m_refValue;

        private Type m_type;
        private int m_repeatNum;
        public Repeater(string valueName, Type type, BBLocation bbType, BBValueType compType, Comparer comp, object refVal)
        {
            m_compType = compType;
            m_valueName = valueName;
            m_comparer = comp;
            m_refValue = refVal;
            m_bbType = bbType;
            m_type = type;
        }
        public Repeater(int repeatNum)
        {
            m_type = Type.CONST;
            m_repeatNum = repeatNum;
        }
    
        public override ITask CreateTask(ITree bt)
        {
            ITask ret = null;
            switch (m_type)
            {
                case Type.CONDITION:
                {
                    IBlackBoard bb = m_bbType == BBLocation.CONTEXT ? bt.GetContext(): bt.GetWorldState();
                    ret = new RepeaterConditionTask(bt,this,bb,BTUtils.PropertyToId(m_valueName));
                    break;
                }
                case Type.CONST:
                {
                 
                    ret = new RepeaterConstTask(bt, this, m_repeatNum);
                    break;
                }
                case Type.MONITOR:
                {
                    IBlackBoard bb = m_bbType == BBLocation.CONTEXT ? bt.GetContext() : bt.GetWorldState();
                    ret = new RepeaterMonitorTask(bt, this, bb, BTUtils.PropertyToId(m_valueName));
                 
                    break;
                }
            }
            return ret;
        }

        public void SetComparerType(BBValueType type)
        {
            m_compType = type;
        }
        public BBValueType GetComparerType()
        {
            return m_compType;
        }
        public void SetComparer(Comparer comp)
        {
            m_comparer = comp;
        }
        public Comparer GetComparer()
        {
            return m_comparer;
        }
        public object GetRefValue()
        {
            return m_refValue;
        }
    }
}
