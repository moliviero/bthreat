﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class Tree : ITree
	{
        private IBlackBoard m_context;
        private IBlackBoard m_worldState;
        private IScheduler m_scheduler;

        private INode m_root;
        private ITask m_rootTask;

        public IBlackBoard GetContext()
        {
            return m_context;
        }

        public void SetContext(IBlackBoard bb)
        {
            m_context = bb;
        }

        public IBlackBoard GetWorldState()
        {
            return m_worldState;
        }

        public void SetWorldState(IBlackBoard bb)
        {
            m_worldState = bb;
        }

        public INode GetRoot()
        {
            return m_root;
        }

        public void SetRoot(INode node)
        {
            m_root = node;
        }

        public void Reset()
        {
            m_scheduler.Clear();
        }

        public void Start()
        {
            m_rootTask = m_root.CreateTask(this);
            m_scheduler.Schedule(m_rootTask);
        }


        public IScheduler GetScheduler()
        {
            return m_scheduler;
        }

        public void SetScheduler(IScheduler scheduler)
        {
            m_scheduler = scheduler;
        }


        public Status Tick(bool autoReset)
        {
            if (m_scheduler.HasTasks())
            {
                m_scheduler.Update();
                return Status.RUNNING;
            }
            if (autoReset)
            {
                Reset();
                Start();
                m_scheduler.Update();
                return Status.RUNNING;
            }
            return m_rootTask.GetStatus();
        }


     
    }
}
