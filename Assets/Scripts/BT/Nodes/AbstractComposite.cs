﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BT
{
	public abstract class AbstractComposite : AbstractNode, INodeComposite 
	{
        protected List<INode> m_Children;

        public AbstractComposite()
        {
            m_Children = new List<INode>();
        }
        public INode GetChild(int index)
        {
           Debug.Assert(m_Children != null && m_Children.Count > index, "m_Children empty or null");

           return m_Children[index];
        }

        public void AppendChild(INode n)
        {
            Debug.Assert(m_Children != null, "children not initialized");

            m_Children.Add(n);
        }

        public void RemoveChild(INode n)
        {
            m_Children.Remove(n);
        }

        public void RemoveChildAt(int index)
        {
            m_Children.RemoveAt(index);
        }

        public abstract ITask CreateTask(ITree bt);



        public int ChildCount()
        {
            return m_Children.Count;
        }
    }
}
