﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
    public class ActionSetter : AbstractNode, IAction
	{
        private IBlackBoard m_bb;

        private BBValueType m_valuteType;
        private BBLocation m_bbType;
        private object m_value;
        private string m_valueName;


        public ActionSetter(string valueName, BBLocation bbType, BBValueType type, object val)
        {
            m_valuteType = type;
            m_value = val;
            m_bbType = bbType;
            m_valueName = valueName;
        }
        public ActionSetter(string valueName, IBlackBoard bb, BBValueType type, object val)
        {
            m_valuteType = type;
            m_value = val;
            m_bb = bb;
            m_bbType = BBLocation.MANUAL;
            m_valueName = valueName;

        }
        public ITask CreateTask(ITree bt)
        {
            ITask task = null;
            switch (m_bbType)
            {
                case BBLocation.CONTEXT:
                    {
                        IBlackBoard bb = bt.GetContext();
                        task = new ActionSetterTask(this,bb);
                        break;
                    }
                case BBLocation.MANUAL:
                    {
                        task = new ActionSetterTask(this,m_bb);
                        break;
                    }
                case BBLocation.WORLD:
                    {
                        IBlackBoard bb = bt.GetWorldState();
                        task = new ActionSetterTask(this,bb);
                        
                        break;
                    }
            }
            return task;
            
        }

        public BBValueType GetValueType()
        {
            return m_valuteType;
        }
        public object GetValue()
        {
            return m_value;
        }
        public string GetValueName()
        {
            return m_valueName;
        }
    }
}
