﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BT.Internals;

namespace BT
{
    public class Condition : AbstractNode, INodeLeaf, ICondition
	{
        private BBValueType m_type;
        private Comparer m_comparer;
        private BBLocation m_bbType;
        private IBlackBoard m_bb;

        private string m_valueName;
        private object m_refValue;

        public Condition(string name)
        {
            m_type = BBValueType.BOOL;
            m_comparer = Comparer.EQUAL;
            m_refValue = true;
            m_valueName = name;
            m_bbType = BBLocation.MANUAL;
        }
        public Condition(string name, IBlackBoard bb, BBValueType type, Comparer comp, object refVal)
        {
            m_type = type;
            m_valueName = name;
            m_comparer = comp;
            m_refValue = refVal;
            m_bb = bb;
            m_bbType = BBLocation.MANUAL;
        }
        public Condition(string name, BBLocation bbType, BBValueType type, Comparer comp, object refVal)
        {
            m_type = type;
            m_valueName = name;
            m_comparer = comp;
            m_refValue = refVal;
            m_bbType = bbType;
        }
        public ITask CreateTask(ITree bt)
        {
            ConditionTask task = null;

            switch (m_bbType)
            {
                case BBLocation.CONTEXT:
                {
                    IBlackBoard bb = bt.GetContext();
                    task = new ConditionTask(this, bb, bb.PropertyToId(m_valueName));
                    break;
                }
                case BBLocation.MANUAL:
                {
                    task = new ConditionTask(this, m_bb, m_bb.PropertyToId(m_valueName));
                    break;
                }
                case BBLocation.WORLD:
                {
                    IBlackBoard bb = bt.GetWorldState();
                    task = new ConditionTask(this, bb, bb.PropertyToId(m_valueName));
              
                    break;
                }
            }
            return task;
        }

        public void SetComparerType(BBValueType type)
        {
            m_type = type;
        }
        public BBValueType GetComparerType()
        {
            return m_type;
        }
        public void SetComparer(Comparer comp)
        {
            m_comparer = comp;
        }
        public Comparer GetComparer()
        {
            return m_comparer;
        }
        public object GetRefValue()
        {
            return m_refValue;
        }
        
    }
}
