﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class Action : AbstractNode, IAction
	{
        public const string ACTION_START_SUFFIX = "_start";
        public const string ACTION_TICK_SUFFIX = "_tick";
        public const string ACTION_TERMINATE_SUFFIX = "_terminate";

        private string m_actionName;
      
        //private string m_startMethod;
        //private string m_tickMethod;
        //private string m_terminateMethod;

        public Action(string name/*, string startAction, string tickAction, string terminateAction*/)
        {
            m_actionName = name;
            //m_startMethod = startAction;
            //m_tickMethod = tickAction;
            //m_terminateMethod = terminateAction;
        }
        public string GetActionName()
        {
            return m_actionName;
        }
        public string GetStartMethodName()
        {
            return m_actionName + ACTION_START_SUFFIX;
        }
        public string GetTickMethodName()
        {
            return m_actionName + ACTION_TICK_SUFFIX;
        }
        public string GetTerminateMethodName()
        {
            return m_actionName + ACTION_TERMINATE_SUFFIX;
        }
        public ITask CreateTask(ITree bt)
        {
            return new ActionDelegateTask(bt.GetContext(), this);
        }
    }
}
