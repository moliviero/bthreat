﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BT.Internals;

namespace BT
{
    public class Monitor : AbstractNode, INode, IMonitor
	{
        
        private BBValueType m_type;
        private Comparer m_comparer;
        private IBlackBoard m_bb; //TODO: remove bb from nodes

        private string m_valueName;
        private object m_refValue;

        private BBLocation m_watchedContext;

        public Monitor(IBlackBoard bb, BBLocation watch, string propId, object refVal, BBValueType type, Comparer comp)
        {
            m_bb = bb;
            m_valueName = propId;
            m_refValue = refVal;
            m_type = type;
            m_comparer = comp;
            m_watchedContext = watch;
        }
        public Monitor(string propId,BBLocation watch, object refVal, BBValueType type, Comparer comp)
        {
            m_valueName = propId;
            m_refValue = refVal;
            m_type = type;
            m_comparer = comp;
            m_watchedContext = watch;
        }
        public ITask CreateTask(ITree bt)
        {
            MonitorTask task = null;
            switch (m_watchedContext)
            {
                case BBLocation.CONTEXT:
                {
                    IBlackBoard bb = bt.GetContext();
                    task = new MonitorTask(this,bb,bb.PropertyToId(m_valueName));
                    break;
                }
                case BBLocation.WORLD:
                {
                    IBlackBoard bb = bt.GetWorldState();
                    task = new MonitorTask(this,bb,bb.PropertyToId(m_valueName));
                    break;
                }
                case BBLocation.MANUAL:
                {
                    task = new MonitorTask(this,m_bb,m_bb.PropertyToId(m_valueName));
                    break;
                
                }
            }
            return task;
        }
        public void SetComparerType(BBValueType type)
        {
            m_type = type;
        }
        public BBValueType GetComparerType()
        {
            return m_type;
        }
        public void SetComparer(Comparer comp)
        {
            m_comparer = comp;
        }
        public Comparer GetComparer()
        {
            return m_comparer;
        }
        public object GetRefValue()
        {
            return m_refValue;
        }
    }
}
