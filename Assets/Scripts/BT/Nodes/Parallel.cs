﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public class Parallel : AbstractComposite, IParallel
	{
        private int m_failNum;
        public int FailNum { get { return m_failNum; } }
        private int m_successNum;
        public int SuccessNum { get { return m_successNum; } }

        public Parallel(int failNum, int successNum)
        {
            m_failNum = failNum;
            m_successNum = successNum;
        }
        public override ITask CreateTask(ITree bt)
        {
            return new ParallelTask(bt,this);
        }
    }
}
