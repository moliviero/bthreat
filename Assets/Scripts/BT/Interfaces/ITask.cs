﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BT
{
	public interface ITask
	{
        event Action<ITask> Started;
        event Action<ITask> Updated;
        event Action<ITask> Terminated;

        Status GetStatus();
        void SetStatus(Status s);

        ITask GetParent();
        void SetParent(ITask parent);

        void SetScheduler(IScheduler scheduler);

        void SetVisitor(ITaskVisitor visitor);
        void Accept(ITaskVisitor visitor);

        void Start();
        Status Tick();
        void Terminate();

        INode SkeletonNode();

        void ClearObservers();
       
	}
}
