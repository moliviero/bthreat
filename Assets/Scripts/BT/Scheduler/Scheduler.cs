﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace BT
{
	public class Scheduler : IScheduler
	{

        public event Action<ITask> OnRemove = delegate{};
        public event Action<ITask> OnUpdateTask = delegate { };

        private List<ITask> m_running;


        private bool m_debug;
        private List<ITask> m_removedTask;
        public List<ITask> GetTerminatedTasks()
        {
            if (m_removedTask != null)
                return m_removedTask;
            return new List<ITask>();
        }

        public Scheduler(bool debug = false) 
        {
            m_running = new List<ITask>();

            if (debug)
            {
                m_removedTask = new List<ITask>();
                OnRemove += TrackTerminatedTask;
            }
        }
        public bool Schedule(ITask task)
        {
            task.SetScheduler(this);

            PushFront(task);

            task.SetStatus(Status.RUNNING);
            task.Start();

            return true;
        }
        public List<ITask> GetRunningTasks()
        {
            return m_running;
        }
        public bool Resume(ITask task, Status stat)
        {
            bool removed = m_running.Remove(task);
            Debug.Assert(removed, "Scheduler::Resume task not found");

            //Activate(task);
            task.SetStatus(stat);

            PushFront(task);

            return removed;
        }

        public bool Suspend(ITask task)
        {
            Debug.Assert(task.GetStatus() == Status.RUNNING, "Scheduler::Suspend task not running or not in queue");

            task.SetStatus(Status.SUSPENDED);

            return true;
        }

        public bool Abort(ITask task)
        {
            throw new NotImplementedException();
        }

        public bool Halt(ITask task)
        {
            bool removed = m_running.Remove(task);

            task.SetStatus(Status.ABORTED);

            if (removed)
            {
                PushFront(task);            
            }
            
            return true;
        }

        private ITask PopFront()
        {
            Debug.Assert(m_running.Count > 0, "Scheduler::PopFront no taks in queue");

            ITask front = m_running[0];
            m_running.RemoveAt(0);

            return front;
        }
        private void PushFront(ITask task)
        {
            m_running.Insert(0, task);
        }
        private void PushBack(ITask task)
        {
            m_running.Add(task);
        }
        private ITask PopBack()
        { 
            Debug.Assert(m_running.Count > 0, "Scheduler::PopBack no taks in queue");

            int lastIdx = m_running.Count - 1;
            ITask back = m_running[lastIdx];
            m_running.RemoveAt(lastIdx);

            return back;



        }

        private bool Step()
        {
            ITask currTask = PopFront();

            if (currTask == null)
                return false; //marker

            Status currStatus = currTask.GetStatus();

            if (!IsActive(currStatus))
            {
                currTask.Terminate();
                OnRemove(currTask);
                return true;
            }
           
            if (currStatus == Status.RUNNING)
            {
                currStatus = currTask.Tick();
                OnUpdateTask(currTask);
            }
            if (!IsActive(currStatus))
            {
                currTask.Terminate();
                OnRemove(currTask);
             
                return true;
            }

            PushBack(currTask); //task not finished..enqueue again

            return true;

        }
        public void Update()
        {
            PushBack(null); //terminator marker

            while (Step()) ;
        }

        private bool IsActive(Status status)
        {
	        return status != Status.COMPLETED  &&  status != Status.FAILED  &&  status != Status.ABORTED;
        }

        //private void Activate(ITask task)
        //{
        //    Debug.Assert(task.GetStatus() == Status.SUSPENDED, "Scheduler::Activate task is not suspended");
        //    task.SetStatus(Status.RUNNING);
        //}


        public bool HasTasks()
        {
            return m_running.Count > 0;
        }

        private void TrackTerminatedTask(ITask task)
        {
            m_removedTask.Add(task);
        }


        public void Clear()
        {
            while (m_running.Count > 0)
            {
                ITask t = m_running[m_running.Count - 1];
                m_running.RemoveAt(m_running.Count - 1);

                t.ClearObservers();
                t.SetStatus(Status.ABORTED);
           
                t.Terminate();
                OnRemove(t);

            }
        }
    }
}
