﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BT.Test
{
    public class MockLeafNode : AbstractNode, INodeLeaf
    {

        public int m_tickNum;
        public Status m_endStatus;

        public ITask CreateTask(ITree bt)
        {
            MockLeafTask mt = new MockLeafTask();
            mt.m_mockNode = this;
            return mt;
        }
    }
	public class MockLeafTask : AbstractTask
	{
        public MockLeafNode m_mockNode;
        public int m_currTickNum = 0;

        public override void Accept(ITaskVisitor visitor)
        {
            throw new NotImplementedException();
        }

        public override void Start()
        {
            
        }

        public override Status Tick()
        {
            m_currTickNum++;
            if (m_currTickNum >= m_mockNode.m_tickNum)
                m_status = m_mockNode.m_endStatus;
           
            return m_status;
        }

        public override void Terminate()
        {
            _Terminated(this);
        }

        public override INode SkeletonNode()
        {
            throw new NotImplementedException();
        }

    
    }
}
