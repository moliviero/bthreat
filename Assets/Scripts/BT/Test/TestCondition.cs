﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Test
{
	public class TestCondition : MonoBehaviour
	{
        
        void Start()
        {
            IBlackBoard bb = new BlackBoard();
            ITree tree = new Tree();

            int bool1_id = bb.PropertyToId("bool1");
            bb.SetBool(bool1_id, true);

            Condition cond = new Condition("bool1", bb, BBValueType.BOOL, Comparer.EQUAL, true);
            ITask cond_task = cond.CreateTask(tree);

            Status s = cond_task.Tick();
            Debug.Log(s);
            bb.SetBool(bool1_id, false);
            Status s2 = cond_task.Tick();
            Debug.Log(s2);
            Condition cond2 = new Condition("float1", bb, BBValueType.FLOAT, Comparer.GREATER_EQUAL, 3f);
            ITask cond2_task = cond2.CreateTask(tree);

            int float1_id = bb.PropertyToId("float1");
            bb.SetFloat(float1_id, 1f);

            Status s3 = cond2_task.Tick();
            Debug.Log(s3);
            bb.SetFloat(float1_id, 3f);

            Status s4 = cond2_task.Tick();
            Debug.Log(s4);
        }
	}
}
