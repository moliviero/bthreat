﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Test
{
	public class TestMonitor : MonoBehaviour
	{
        IScheduler scheduler;
        IBlackBoard bb;
        INode root;
        int minDistId;
        void Test1_setup()
        {
            ITree tree = new Tree();
            scheduler = new Scheduler();
            bb = new BlackBoard();

            minDistId = bb.PropertyToId("minDist");
            bb.SetInt(minDistId, 1);


            Parallel par1 = new Parallel(1, 2);
            Monitor distMonitor = new Monitor(bb,BBLocation.CONTEXT, "minDist",3,BBValueType.INT,Comparer.LESS);

            par1.AppendChild(distMonitor);
            ITask par_task = par1.CreateTask(tree);

            scheduler.Schedule(par_task);

            root = par1;

        }
        IEnumerator Test1_run(IScheduler s)
        {
            while (s.GetRunningTasks().Count > 0)
            {
                s.Update();
                yield return null;
            }
        }
        void Start()
        {
            StartCoroutine(TestRunner());
        }
        IEnumerator TestRunner()
        {
            Test1_setup();
            yield return StartCoroutine(Test1_run(scheduler));
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                bb.SetInt(minDistId, 3);
            }
        }
	}
}
