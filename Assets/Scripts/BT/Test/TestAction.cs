﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Test
{
	class TestAction : MonoBehaviour
	{
        Scheduler scheduler;
        ITree tree;
        void Test1_setup()
        {
            IBlackBoard bb = new BlackBoard();
            tree = new Tree();
            scheduler = new Scheduler();

            Action act = new Action("MockAction");

            TaskStartAction startDelegate = Delegate.CreateDelegate(typeof(TaskStartAction),this,"OnStartAction") as TaskStartAction;
            TaskTickAction tickDelegate = Delegate.CreateDelegate(typeof(TaskTickAction), this, "OnTickAction") as TaskTickAction;
            TaskTerminateAction terminateDelegate = Delegate.CreateDelegate(typeof(TaskTerminateAction), this, "OnTerminateAction") as TaskTerminateAction;

            int startId = bb.PropertyToId(act.GetStartMethodName());
            int tickId = bb.PropertyToId(act.GetTickMethodName());
            int terminateId = bb.PropertyToId(act.GetTerminateMethodName());

            bb.SetObject<TaskStartAction>(startId, startDelegate);
            bb.SetObject<TaskTickAction>(tickId, tickDelegate);
            bb.SetObject<TaskTerminateAction>(terminateId, terminateDelegate);

            int bool1_id = bb.PropertyToId("bool1");
            bb.SetBool(bool1_id, true);

            tree.SetContext(bb);

            scheduler.Schedule(act.CreateTask(tree));
        }
        IEnumerator Test1_run(Scheduler s)
        {
            //            Debug.Log("r " + s.GetRunningTasks().Count);
            while (s.GetRunningTasks().Count > 0)
            {
                s.Update();
                yield return null;
            }
        }
        void Start()
        {
            StartCoroutine(TestRunner());
        }
        IEnumerator TestRunner()
        {
            Test1_setup();
            yield return StartCoroutine(Test1_run(scheduler));
        }
        private void OnStartAction()
        {
            Debug.Log("start");
        }
        private int tickNum = 0;
        private Status OnTickAction()
        {
            tickNum++;
            Debug.Log("act " + tickNum);
            if (tickNum > 3)
                return Status.COMPLETED;
            return Status.RUNNING;
        }
        private void OnTerminateAction()
        {
            Debug.Log("terminated");
        
        }
	}
}
