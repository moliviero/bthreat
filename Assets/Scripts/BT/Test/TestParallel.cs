﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Test
{
    public class TestParallel : MonoBehaviour
	{
        Scheduler scheduler;
        INode root;
        void Test1_setup()
        {
            ITree tree = new Tree();
            scheduler = new Scheduler();

            Parallel par1 = new Parallel(1,2);
            MockLeafNode m1 = new MockLeafNode();
            m1.m_endStatus = Status.COMPLETED;
            m1.m_tickNum = 3;

            MockLeafNode m2 = new MockLeafNode();
            m2.m_endStatus = Status.FAILED;
            m2.m_tickNum = 2;

            par1.AppendChild(m1);
            par1.AppendChild(m2);

            ITask par_task = par1.CreateTask(tree);

            scheduler.Schedule(par_task);

            root = par1;

        }
        IEnumerator Test1_run(Scheduler s)
        {
            while (s.GetRunningTasks().Count > 0)
            {
                s.Update();
                yield return null;
            }
        }
        void Start()
        {
            StartCoroutine(TestRunner());
        }
        IEnumerator TestRunner()
        {
            Test1_setup();
            yield return StartCoroutine(Test1_run(scheduler));
        }
	}
}
