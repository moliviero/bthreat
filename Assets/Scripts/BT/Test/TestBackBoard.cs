﻿/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BT.Test
{
	public class TestBackBoard : MonoBehaviour
	{
        public class MockObj
        {
            public string name;
        }
        void Start()
        {
            IBlackBoard bb = new BlackBoard();

            int fooId = bb.PropertyToId("Foo");
//            int barId = bb.PropertyToId("Bar");

            bb.SetBool(fooId, true);

//            bool fooVal = bb.GetBool(fooId);

          
            MockObj mock = new MockObj();
            mock.name = "test_obj";

            int mockId = bb.PropertyToId(mock.name);

            bb.ObserverSubscribe(mockId, OnObjectChange);
            bb.ObserverSubscribe(mockId, OnObjectChange2);

            bb.SetObject<MockObj>(mockId, mock);

            bb.ObserverUnsubscribe(mockId,OnObjectChange2);

            MockObj mock2 = new MockObj();
            mock.name = "test_obj_2";

            bb.SetObject<MockObj>(mockId, mock2);

        }
        private void OnObjectChange(IBlackBoard bb, int id)
        { 
            MockObj mock = bb.GetObject<MockObj>(id);
            Debug.Log("Change " + mock.name);
        }
        private void OnObjectChange2(IBlackBoard bb, int id)
        {

            MockObj mock = bb.GetObject<MockObj>(id);
            Debug.Log("Change2 " + mock.name);
        }
	}
}
