/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using EnumerableExtension;

public static class UnityUtils 
{
	
	//TODO: to be tested
	public static IEnumerable<GameObject> FindObjectsInLayer(int layer)
	{
		IEnumerable<GameObject> goList = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];
		
		return goList.Where(obj => obj.layer == layer);
	}
	
	public static GameObject FindChildWithName(GameObject parent, string name)
	{
		Transform[] children = parent.GetComponentsInChildren<Transform>();
		
		return children.Where(c => c.gameObject.name.Equals(name)).Select(t => t.gameObject).First();
	}
	public static GameObject FindChildWithTag(GameObject parent, string tagName)
	{
		Transform[] children = parent.GetComponentsInChildren<Transform>();
		
		return children.Where(c => c.gameObject.CompareTag(tagName)).Select(t => t.gameObject).First();
	}
	
	public static List<Type> GetAllComponentsInChildren<Type> (Transform root, Func<Type,bool> func)
		where Type : Component
	{
		List<Type> list = new List<Type>();////Enumerable.Empty<Type>();
		
		Type comp = root.gameObject.GetComponent<Type>(); 
		if (comp != null && func(comp))
			list.Add(comp);
		foreach(Transform child in root)
		{
			List<Type> childComps = GetAllComponentsInChildren<Type>(child, func);
			//Type currComponent = child.GetComponent<Type>();
			list.AddRange(childComps);
		}
		return list;
	}
	public static IInterface GetComponentImplementingInterface<IInterface>(GameObject go)
		where IInterface : class
	{
		Component[] components = go.GetComponents<Component>();
		IInterface i = null;
		foreach(Component c in components)
		{
			if (c is IInterface)
			{
				i = c as IInterface;
				
				break;
			}
		}
		return i;
	}
	
//	public static Transform FindRoot()
	
	//public float Derivative(Animation )
	public static float RungeKutta(float time, float timeMin, Func<float,float> Sigma, Func<float,float> Derivative, int stepNum)
	{

		float h = (time - timeMin)/ stepNum;
		float u = 0f;
		time = timeMin;
		
		for(int i = 0; i< stepNum; ++i)
		{
			float k1 = h * Sigma(time)/ Derivative(u);
			float k2 = h * Sigma(time + h *0.5f)/Derivative(u + k1*0.5f);
			float k3 = h * Sigma(time + h *0.5f)/Derivative(u + k2*0.5f);
			float k4 = h * Sigma(time + h)/Derivative(u + k3);
			
			time +=h;
			u += (k1 + 2*(k2 + k3) + k4) /6;
			
		}
		return u;
	
	}

	public static float Integral(AnimationCurve curve, float step)
	{
		float timeMin = curve.keys.First().time;
		float time = curve.keys.Last().time - timeMin;
		
		return RungeKutta(time, timeMin, curve.Evaluate, 
		t => 
		{   Debug.Log("Derivative " + curve.Evaluate(t)); 
			return curve.Evaluate(t) / t;
		},10);
		
	}


}

