/* Copyright (C) 2014-2016 Marco Oliviero.

This file is part of BThreat.

BThreat is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

BThreat is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with BThreat. If not, see<http://www.gnu.org/licenses/>. */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EnumerableExtension
{
public static class EnumerableExtension{
	/// <summary>Adds a single element to the end of an IEnumerable.</summary>
	/// <typeparam name="T">Type of enumerable to return.</typeparam>
	/// <returns>IEnumerable containing all the input elements, followed by the
	/// specified additional element.</returns>
	public static IEnumerable<T> Append<T>(this IEnumerable<T> source, T element)
	{
	    if (source == null)
	        throw new ArgumentNullException("source");
	    return concatIterator(element, source, false);
	}
	
	/// <summary>Adds a single element to the start of an IEnumerable.</summary>
	/// <typeparam name="T">Type of enumerable to return.</typeparam>
	/// <returns>IEnumerable containing the specified additional element, followed by
	/// all the input elements.</returns>
	public static IEnumerable<T> Prepend<T>(this IEnumerable<T> tail, T head)
	{
	    if (tail == null)
	        throw new ArgumentNullException("tail");
			
		Debug.Log("AT LEST");
	    	
	    return concatIterator(head, tail, true);
	}
	
		
	private static IEnumerable<T> concatIterator<T>(T extraElement, IEnumerable<T> source, bool insertAtStart)
	{
		Debug.Log("AT LEST");
	    if (insertAtStart)
	        yield return extraElement;
	    foreach (var e in source)
	        yield return e;
	    if (!insertAtStart)
	        yield return extraElement;
	}
}
}